package com.lanhai.agv.listener;

import java.net.DatagramPacket;
import java.net.DatagramSocket;


/**
 * 第三方侦听
 * @author virus408
 *
 */
public class ThirdListener {

	private boolean running;
	private Thread th;
	
	public void Start(int port) {
		running=true;
		th=new Thread() { 
			@Override
			public void run() {
				try{ 
					DatagramSocket	ds = new DatagramSocket(port);
					while(running){
						byte buf[] = new byte[20];
						DatagramPacket dp = new DatagramPacket(buf, buf.length);
						ds.receive(dp);
						//什么都不需要做
					}	
					ds.close();
				}catch(Exception e){
					e.printStackTrace();
				}	
			}
			
		};
		th.start();
	}
	 
	
	public void Stop() {
		running=false;
		try {
			th.interrupt();
			th=null; 
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
