package com.lanhai.agv.listener;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import com.lanhai.agv.queue.DispatchQueue;
import com.lanhai.agv.queue.DispatchUnit;
import com.lanhai.agv.queue.TargetCmdEnum;

/**
 * 来自后台管理的指令
 * @author virus408
 *
 */
public class HackListener {
	private boolean running;
	private Thread th;
	
	public void Start(int port) {
		running=true;
		th=new Thread() { 
			@Override
			public void run() {
				try{ 
					DatagramSocket	ds = new DatagramSocket(port);
					while(running){
						byte buf[] = new byte[20];
						DatagramPacket dp = new DatagramPacket(buf, buf.length);
						ds.receive(dp);
					 
						DispatchUnit unit=new DispatchUnit();
						unit.setData(buf);
						unit.setTarget(TargetCmdEnum.StationState);
						unit.setIp(dp.getAddress().getHostAddress()); 
						DispatchQueue.Push(unit);
					}	
					ds.close();
				}catch(Exception e){
					e.printStackTrace();
				}	
			}
			
		};
		th.start();
	}
	 
	
	public void Stop() {
		running=false;
		try {
			th.interrupt();
			th=null; 
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
