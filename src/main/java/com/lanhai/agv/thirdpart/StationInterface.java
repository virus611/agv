package com.lanhai.agv.thirdpart;



/**
 * 对接设备事件定义,所有的对接机都必须实现此类
 * @author virus408
 *
 */
public interface StationInterface {
	
	byte[] applyTransfer(ApplyTransferEnum type);
	
	byte[] clear( );
	
	byte[] applyLeave( );
	
	StationData resoleveData(byte[] buf);
}
