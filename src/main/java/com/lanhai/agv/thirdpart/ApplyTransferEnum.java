package com.lanhai.agv.thirdpart;



/**
 * 申请传输的类型
 * @author virus408
 *
 */
public enum ApplyTransferEnum {
	In,
	Out,
	Both
}
