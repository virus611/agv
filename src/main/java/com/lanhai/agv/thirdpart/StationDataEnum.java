package com.lanhai.agv.thirdpart;

public enum StationDataEnum {
	
	HasGift("设备有货"),
	AllowTrans("允许传输"),
	Error("设备异常"),
	AllowLeave("允许离开");
	
	StationDataEnum(String string) {
		eventName=string;
	}
	
	

    private String eventName;
 
	

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
			
}
