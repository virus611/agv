package com.lanhai.agv.thirdpart;

import com.lanhai.agv.Tools;

/**
 * 罗博特科 PE-背膜 和 PE-正膜 
 * @author virus408
 *
 */
public class Robo implements StationInterface {

	@Override
	public byte[] applyTransfer(ApplyTransferEnum type) {
		byte[] buf = new byte[] { 0x1A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		if (type == ApplyTransferEnum.In) {
			// 进料
			buf[2] = 0x01;
			buf[4] = 0x00;
		} else if (type == ApplyTransferEnum.Out) {
			// 出料
			buf[2] = 0x00;
			buf[4] = 0x01;
		} else { 
			buf[2] = 0x01;
			buf[4] = 0x01;
		}
		return buf;
	}

	@Override
	public byte[] clear() {
		  return  new byte[] { 0x1A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    }
 

	@Override
	public byte[] applyLeave() {
		  return new byte[] { 0x1A, 0x00, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00 };
	}

	@Override
	public StationData resoleveData(byte[] buf) {
		StationData obj=new StationData();
		obj.setCmdStr(Tools.byte2HexStr(buf));
		if (buf[2] == 0x04|| buf[4] == 0x04)
        {
            //机台异常
			obj.setEventType(StationDataEnum.Error);
            return  obj;
        }
        if (buf[2] == 0x03 || buf[4] == 0x03)
        {
            //允许 离开
        	obj.setEventType(StationDataEnum.AllowLeave); 
       	 return  obj;
        }
        if (buf[2] == 0x02 || buf[4] == 0x02)
        {
            //允许 传输
        	obj.setEventType(StationDataEnum.AllowTrans); 

        }
        
        if (buf[2] == 0x01)
        {
            //进料
        	   obj.setNeedIn(true);
        	    obj.setInNum(10);
        }
        if (buf[4] == 0x01)
        {
            //出料
        	 obj.setNeedOut(true);
        	 obj.setOutNum(10);
        }
    
       
        return  obj;
	}

}
