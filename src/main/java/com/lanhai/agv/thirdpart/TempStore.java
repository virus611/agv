package com.lanhai.agv.thirdpart;

/**
 * 发料机 蓝海
 * @author virus408
 *
 */
public class TempStore implements StationInterface{

	@Override
	public byte[] applyTransfer(ApplyTransferEnum type) {
		byte[] buf = new byte[32];
		buf[1] = 1;
		if (type == ApplyTransferEnum.In) {
			// 上层进料
			buf[3] = 0x01;
		} else if (type == ApplyTransferEnum.Out) {
			// 下层出料
			buf[5] = 0x01;
		} else { 
			buf[3] = 0x01;
			buf[5] = 0x01;
		} 
		return buf;
	}

	@Override
	public byte[] clear() {
		byte[] buf=new byte[32];
		  buf[1]=1;
		  return buf;
	}

	@Override
	public byte[] applyLeave() {
		byte[] buf=new byte[32];
		  buf[1]=1;
		  buf[3]=2;
		  buf[5]=2;
		  return buf;
	}

	@Override
	public StationData resoleveData(byte[] buf) {
		StationData data=new StationData();
		if(buf[5]==0) {
			data.setEventType(StationDataEnum.Error) ;
			return data;
		}
		if(buf[1]==1) {
			//上层可出料
			data.setOutNum(10);
			data.setNeedOut(true);
		}
		if(buf[3]==1) {
			data.setInNum(10);
			data.setNeedIn(true);
		} 
		return data;
	}

}
