package com.lanhai.agv.thirdpart;

 
/**
 * 对接设备上报的数据
 * 
 * @author virus408
 *
 */
public class StationData {
	private StationDataEnum eventType; 
	private boolean needIn;
	private boolean needOut;

	private String cmdStr;
	private int inNum;
	private int outNum;
	
	public String getCmdStr() {
		return cmdStr;
	}

	public void setCmdStr(String cmdStr) {
		this.cmdStr = cmdStr;
	}

	public StationDataEnum getEventType() {
		return eventType;
	}

	public void setEventType(StationDataEnum eventType) {
		this.eventType = eventType;
	}

	 

	public boolean isNeedIn() {
		return needIn;
	}

	public void setNeedIn(boolean needIn) {
		this.needIn = needIn;
	}

	public boolean isNeedOut() {
		return needOut;
	}

	public void setNeedOut(boolean needOut) {
		this.needOut = needOut;
	}

	public int getInNum() {
		return inNum;
	}

	public void setInNum(int inNum) {
		this.inNum = inNum;
	}

	public int getOutNum() {
		return outNum;
	}

	public void setOutNum(int outNum) {
		this.outNum = outNum;
	}

	 
	 

}
