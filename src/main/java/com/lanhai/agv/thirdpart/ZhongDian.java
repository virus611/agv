package com.lanhai.agv.thirdpart;

import com.lanhai.agv.Tools;

/**
 * 中电二所，制绒
 * @author virus408
 *
 */
public class ZhongDian implements StationInterface{

	@Override
	public byte[] applyTransfer(ApplyTransferEnum type) {
		byte[] buf = new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		if (type == ApplyTransferEnum.In) {
			// 进料
			buf[1] = 0x01;
			buf[2] = 0x00;
		} else if (type == ApplyTransferEnum.Out) {
			// 出料
			buf[1] = 0x00;
			buf[2] = 0x01;
		} else { 
			buf[1] = 0x01;
			buf[2] = 0x01;
		}

		return buf;
	}

	@Override
	public byte[] clear() {
		  return new byte[] { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		     
	}

	@Override
	public byte[] applyLeave() {
	  return new byte[ ] { 0x01, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00 };

	}

	@Override
	public StationData resoleveData(byte[] buf) {
		StationData obj=new StationData();
		obj.setCmdStr(Tools.byte2HexStr(buf));
		 if (buf[1] == 0x02)
         {
             //机台异常
				obj.setEventType(StationDataEnum.Error);
	            return  obj;
         }
         if (buf[2] == 0x03 || buf[3] == 0x03)
         {
             //允许 离开
        	 obj.setEventType(StationDataEnum.AllowLeave); 
        	 return  obj;
         }
         if (buf[2] == 0x02 || buf[3] == 0x02)
         {
        	    //允许 传输
         	obj.setEventType(StationDataEnum.AllowTrans); 

             
         }
         
         if (buf[2] == 0x01)
         {
             //进料
        	   obj.setNeedIn(true);
        	   obj.setInNum(10);
         }
         if (buf[3] == 0x01)
         {
             //出料
        	 obj.setNeedOut(true);
             obj.setOutNum(10);
         }
       
         return  obj;
	}

}
