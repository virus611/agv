package com.lanhai.agv.thirdpart;
 



/**
 * 解析工厂
 * @author virus408
 *
 */
public class StationResolveFactory {
	/*
	static Map<String,String> map=new HashMap<String,String>();
	
	static {
		map.put("mw", "MaiWei");
	}
	*/
	
	
	 
	public static StationInterface getStationResolve(String name) {
		 
		if(name.equals("MW")) {
			return new  MaiWei();
		}else if(name.equals("JS_Q")) {
			return new JSongQ();
		}else if(name.equals("JS_Y")) {
			return new JSongY();
		}else if(name.equals("JS_W")) {
			return new JSongW();
		}else if(name.equals("ZDES")) {
			return new ZhongDian();
		}else if(name.equals("ROBO")) {
			return new Robo();
		}else if(name.equals("WD")) {
			return new WeiDao();
		}else if(name.equals("DR")) {
			return new DrLaser();
		}else if(name.equals("LH")) {
			return new TempStore();
		}  
		return null;
		
		/*
		String m=map.get(name);
		if(m.isEmpty()) {
			 return null;
		}
		String cname="com.lanhai.agv.thirdpart."+m;
		  
			try {
				Class<?> classDemo = Class.forName(cname);
				Object obj=classDemo.newInstance();
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 return null;
		 */
		
	}
}
