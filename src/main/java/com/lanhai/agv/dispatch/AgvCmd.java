package com.lanhai.agv.dispatch;

/**
 * 生成小车指令串
 * 
 * @author virus408
 *
 */
public class AgvCmd {

	/**
	 * 执行某路或者回归主路
	 * @param agvid
	 * @param road
	 * @return
	 */
	public static byte[] runRoad(int agvid, int road) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 1;
		buf[3] = (byte) road;

		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}

	/**
	 * 停止或者启动
	 * @param agvid
	 * @param stop
	 * @return
	 */
	public static byte[] stopOrstart(int agvid, boolean stop) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 3;
		if (stop) {
			buf[5] = 1;
		} else {
			buf[5] = 2;
		}

		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}

	/**
	 * 上层传输
	 * @param agvid
	 * @param in
	 * @return
	 */
	public static byte[] transUp(int agvid,boolean receive) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 2;
		
		if(receive) {
			//上层传入
			buf[4]=3;
		}else {
			//上层传出
			buf[4]=2;
		}
		
		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}
	
	/**
	 * 下层传输
	 * @param agvid
	 * @param receive
	 * @return
	 */
	public static byte[] transDown(int agvid,boolean receive) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 2;
		
		if(receive) {
			//下层传入
			buf[4]=5;
		}else {
			//下层传出
			buf[4]=4;
		}
		
		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}
	
	/**
	 * 上出下入 6、上出下入 
	 * @param agvid
	 * @return
	 */
	public static byte[] transUpOutAndDownIn(int agvid ) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 2;
		

		 buf[4]=6;
		
		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}
	
	/**
	 * 上入下出 7、上入下出
	 * @param agvid
	 * @return
	 */
	public static byte[] transUpInAndDownOut(int agvid ) {
		byte[] buf = new byte[8];
		buf[0] = (byte) 0xA1;
		buf[1] = (byte) agvid;
		buf[2] = 2;
		 
		 buf[4]=7;
		 
		
		byte s = 0;
		for (int i = 0; i < 7; i++) {
			s += buf[i];
		}
		buf[7] = s;
		return buf;
	}
}
