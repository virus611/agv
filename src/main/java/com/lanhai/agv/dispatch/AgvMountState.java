package com.lanhai.agv.dispatch;

/**
 * 小车装载情况
 * 
 * @author virus408
 *
 */
public class AgvMountState {
	private boolean upFull;
	private boolean downFull;
	private boolean finishSend;
	private boolean finishReceive;

	public boolean isUpFull() {
		return upFull;
	}

	public void setUpFull(boolean upFull) {
		this.upFull = upFull;
	}

	public boolean isDownFull() {
		return downFull;
	}

	public void setDownFull(boolean downFull) {
		this.downFull = downFull;
	}

	public boolean isFinishSend() {
		return finishSend;
	}

	public void setFinishSend(boolean finishSend) {
		this.finishSend = finishSend;
	}

	public boolean isFinishReceive() {
		return finishReceive;
	}

	public void setFinishReceive(boolean finishReceive) {
		this.finishReceive = finishReceive;
	}

}
