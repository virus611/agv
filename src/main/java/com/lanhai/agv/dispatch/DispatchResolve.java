package com.lanhai.agv.dispatch;

import com.lanhai.agv.Tools;
import com.lanhai.agv.cache.AgvCache;
import com.lanhai.agv.cache.AgvItem;
import com.lanhai.agv.cache.EstimateCache; 
import com.lanhai.agv.cache.MountCache;
import com.lanhai.agv.cache.ParkCache;
import com.lanhai.agv.cache.ParkItem;
import com.lanhai.agv.cache.ParkRelationCache;
import com.lanhai.agv.cache.ReadyDotCache;
import com.lanhai.agv.cache.ReadyDotItem;
import com.lanhai.agv.cache.StationGroupCache;
import com.lanhai.agv.queue.DispatchUnit;

/**
 * 内部调度处理
 * @author virus408
 *
 */
public class DispatchResolve {

	public static void Resolve(DispatchUnit unit) {
		switch(unit.getTarget()) {
		case Judgement:
			//传输调度
			ParkItem park=null;
			if(unit.isFromAgv()) {
				park=ParkCache.getParkByAgv(unit.getIp());
			}else {
				park=ParkCache.getParkByStation(unit.getIp(), unit.getPort());
			}
			if(park==null) {
				return;
			}
			//String msg=	MountCache.judgement(park.getAgvIP(), park);
			MountCache.judgement(park.getAgvIP(), park);
			break;
		
		case SelectRoad:
			//路线选择,目前不存在
			break;
		case StationState:
			//响应后台指令
			//第1位，0x00表示启用 之后四位是点位编号 
			byte[] bf=unit.getData();
			if(bf.length>6) {
				boolean disable=bf[0]>0;
				//999= E7 03 00 00
				int parkNo=Tools.toIntL(new byte[] {bf[1],bf[2],bf[3],bf[4]}); 
				ParkItem pk=ParkCache.getParkByNo(parkNo );
				if(pk==null) return;
				
				StationGroupCache.updateDisable(pk.getProcessGroup(),pk.getStationGroup(), disable); 
			} 
			 break;
		case NoticeReadyDot:
			////释放了对接设备，分配下一小车进入
			//通知对应的装卸分配点有空机台可用
			int parkNo=unit.getParkNo();
			String process= unit.getProcessName();
			ReadyDotItem dot= ReadyDotCache.getFirstAgv(parkNo,process);
			if(dot==null) {
				//没有小车
				return;
			}
			//通知小车回到装卸判断位
			AgvItem ag=AgvCache.getAgvByPark(dot.getParkNo() );
			AgvResolve.AgvToRoad(ag, dot.getTargetRoad()); 
			 
			break;
		case NoticeEstimate:
			////通知预判断点放行小车
			int estimateNo=ParkRelationCache.getEstimateParkNo(unit.getParkNo());
			 if(estimateNo<=0);
			int agvid=	EstimateCache.nextAgv(estimateNo);
			if(agvid==0) {
				//没有野车
				return;
			}
			//让该小车进入预判断位
			AgvItem agv2=AgvCache.getAgvByID(agvid);
			if(agv2!=null) {
				int road=ParkCache.getTargetRoad(estimateNo);
				AgvResolve.AgvToRoad(agv2, road); 
			}
			
			break;
		case Nono:
			break;
		default:
			break;
		
		}
	}
	
}
