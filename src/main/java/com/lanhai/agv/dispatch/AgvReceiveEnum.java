package com.lanhai.agv.dispatch;

/**
 * 小车上报数据的类型
 * @author virus408
 *
 */
public enum AgvReceiveEnum {
	XS(1, "行驶"),
	DD(2, "到达"),
	KCS(3, "可传输"),
	CD(4, "充电"),
	JG(5, "交管"),
	TZ(6, "停止"),
	BZ(7, "避障"),
	JT(8, "急停"),
	SCC(9, "上层传出"),
	SCCW(10, "上层传出完成"),
	SCR(11, "上层传入"),
	SCRW(12, "上层传入完成"),
	XCC(13, "下层传出"),
	XCCW(14, "下层传出完成"),
	XCR(15, "下层传入"),
	XCRW(16, "下层传入完成"),
	SCXR(17, "上出下入中"),
	SCXRW(18, "上出下入完成"),
	SRXC(19, "上入下出中"),
	SRXCW(20, "上入下出完成"),
	SZC(21, "上左传出"),
	SZCW(22, "上左传出完成"),
	SZR(23, "上左传入"),
	SZRW(24, "上左传入完成"),
	SRC(25, "上右传出"),
	SRCW(26, "上右传出完成"),
	SRR(27, "上右传入"),
	SRRW(28, "上右传入完成"),
	XZC(29, "下左传出"),
	XZCW(30, "下左传出完成"),
	XZR(31, "下左传入"),
	XZRW(32, "下左传入完成"),
	XRC(33, "下右传出"),
	XRCW(34, "下右传出完成"),
	XRR(35, "下右传入"),
	XRRW(36, "下右传入完成"),
	DM(37, "待命"),
	LX(38, "离线"),
	ZXWC(39, "装卸完成"),
	
	ERR(1000, "异常");
	
	
	AgvReceiveEnum(int i, String string) {
		code=i;
		name=string;
	}
	private int code;
	private String name;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static AgvReceiveEnum getAgvReceiveEnumByCode(byte code){
    	AgvReceiveEnum as = null;
    	switch (code) {
    	case 1:
    		as = AgvReceiveEnum.XS;//行驶
    		break;
    	case 2:
    		as = AgvReceiveEnum.DD;//到达
    		break;
    	case 3:
    		as = AgvReceiveEnum.KCS;//可传输
    		break;
    	case 4:
    		as = AgvReceiveEnum.CD;//充电
    		break;
    	case 5:
    		as = AgvReceiveEnum.JG;//交管
    		break;
    	case 6:
    		as = AgvReceiveEnum.TZ;//停止
    		break;
    	case 7:
    		as = AgvReceiveEnum.BZ;//避障
    		break;
    	case 8:
    		as = AgvReceiveEnum.JT;//急停
    		break;
    	case 9:
    		as = AgvReceiveEnum.SCC;//上层传出
    		break;
    	case 10:
    		as = AgvReceiveEnum.SCCW;//上层传出完成
    		break;
    	case 11:
    		as = AgvReceiveEnum.SCR;//上层传入
    		break;
    	case 12:
    		as = AgvReceiveEnum.SCRW;//上层传入完成
    		break;
    	case 13:
    		as = AgvReceiveEnum.XCC;//下层传出
    		break;
    	case 14:
    		as = AgvReceiveEnum.XCCW;//下层传出完成
    		break;
    	case 15:
    		as = AgvReceiveEnum.XCR;//下层传入
    		break;
    	case 16:
    		as = AgvReceiveEnum.XCRW;//下层传入完成
    		break;
    	case 17:
    		as = AgvReceiveEnum.SCXR;//上出下入
    		break;
    	case 18:
    		as = AgvReceiveEnum.SCXRW;//上出下入
    		break;    	
    	case 19:
    		as = AgvReceiveEnum.SRXC;//上入下出中
    		break;      		
    	case 20:
    		as = AgvReceiveEnum.SRXCW;//上入下出完成
    		break;
    	case 21:
    		as = AgvReceiveEnum.SZC;//上左传出
    		break;
    	case 22:
    		as = AgvReceiveEnum.SZCW;//上左传出完成
    		break;    		
    	case 23:
    		as = AgvReceiveEnum.SZR;//上左传入
    		break;    		
    	case 24:
    		as = AgvReceiveEnum.SZRW;//上左传入完成
    		break;    		
    	case 25:
    		as = AgvReceiveEnum.SRC;//上右传出
    		break;    	
    	case 26:
    		as = AgvReceiveEnum.SRCW;//上右传出完成
    		break;
    	case 27:
    		as = AgvReceiveEnum.SRR;//上右传入
    		break;    		
    	case 28:
    		as = AgvReceiveEnum.SRRW;//上右传入完成
    		break;    		
    	case 29:
    		as = AgvReceiveEnum.XZC;//下左传出
    		break;    		
    	case 30:
    		as = AgvReceiveEnum.XZCW;//下左传出完成
    		break;    		
    	case 31:
    		as = AgvReceiveEnum.XZR;//下左传入
    		break;    	
    	case 32:
    		as = AgvReceiveEnum.XZRW;//下左传入完成
    		break;
    	case 33:
    		as = AgvReceiveEnum.XRC;//下右传出
    		break;    		
    	case 34:
    		as = AgvReceiveEnum.XRCW;//下右传出完成
    		break;    		
    	case 35:
    		as = AgvReceiveEnum.XRR;//下右传入
    		break;    		
    	case 36:
    		as = AgvReceiveEnum.XRRW;//下右传入完成
    		break;
    	case 37:
    		as = AgvReceiveEnum.DM;//待命
    		break;    	    		
    	case 38:
    		as = AgvReceiveEnum.LX;//离线
    		break;    	    	  
    	case 39:
    		as = AgvReceiveEnum.ZXWC;//装卸完成
    		break;
    	default:
    			as=AgvReceiveEnum.ERR;
    			break;
    	}
    	return as;
    }
}
