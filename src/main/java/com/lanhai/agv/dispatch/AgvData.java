package com.lanhai.agv.dispatch;

/**
 * agv上报的数据
 * @author virus408
 *
 */
public class AgvData {
	private AgvReceiveEnum type;
	private int x;
	private int y;
	private int agvId;
	private int power;
	private int road;
	
	private boolean upFull;
	private boolean downFull;
	
	private String cmdStr;
	private byte[] data;
	
	
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public AgvReceiveEnum getType() {
		return type;
	}
	public void setType(AgvReceiveEnum type) {
		this.type = type;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getAgvId() {
		return agvId;
	}
	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getRoad() {
		return road;
	}
	public void setRoad(int road) {
		this.road = road;
	}
	public boolean isUpFull() {
		return upFull;
	}
	public void setUpFull(boolean upFull) {
		this.upFull = upFull;
	}
	public boolean isDownFull() {
		return downFull;
	}
	public void setDownFull(boolean downFull) {
		this.downFull = downFull;
	}
	public String getCmdStr() {
		return cmdStr;
	}
	public void setCmdStr(String cmdStr) {
		this.cmdStr = cmdStr;
	}
	
	
}
