package com.lanhai.agv.dispatch;

import com.lanhai.agv.queue.StationEventQueue;
import com.lanhai.agv.queue.StationEventUnit;

/**
 * 对接设备队列事件消费
 * @author virus408
 *
 */
public class StationEventThread {

	boolean running;
	Thread[] th;

	public void Start(int num) {
		th = new Thread[num];
		running = true;
		for (int i = 0; i < num; i++) {
			th[i] = new Thread() {

				@Override
				public void run() {
					while (running) {
						try {
							StationEventUnit item = StationEventQueue.Pop();
							if (item == null) {
								Thread.sleep(100);
							} else {
								StationResolve.Resolve(item);
							}
 
						} catch (Exception e) {

						}
					}
				}

			};
			th[i].start();
		}
	}

	public void Stop() {
		running = false;
		for (int i = 0; i < th.length; i++) {
			try {
				th[i].interrupt();
				th[i] = null;
			} catch (Exception e) {

			}
		}
	}

}
