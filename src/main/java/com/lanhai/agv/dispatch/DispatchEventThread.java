package com.lanhai.agv.dispatch;

import com.lanhai.agv.queue.DispatchQueue;
import com.lanhai.agv.queue.DispatchUnit;

/**
 * 调度队列事件消费
 * @author virus408
 *
 */
public class DispatchEventThread {

	boolean running;
	Thread[] th;

	public void Start(int num) {
		th = new Thread[num];
		running = true;
		for (int i = 0; i < num; i++) {
			th[i] = new Thread() {

				@Override
				public void run() {
					while (running) {
						try {
							DispatchUnit item = DispatchQueue.Pop();
							if (item == null) {
								Thread.sleep(100);
							} else {
								DispatchResolve.Resolve(item);
							}
 
						} catch (Exception e) {

						}
					}
				}

			};
			th[i].start();
		}
	}

	public void Stop() {
		running = false;
		for (int i = 0; i < th.length; i++) {
			try {
				th[i].interrupt();
				th[i] = null;
			} catch (Exception e) {

			}
		}
	}

}
