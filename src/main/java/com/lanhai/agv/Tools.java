package com.lanhai.agv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Tools {
	
	
	public static String byte2HexStr(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs += " 0" + stmp;
			else
				hs += " " + stmp;  
		}
		return hs.toUpperCase().substring(1);
	}
	
	public static short toIntL(byte[] b) {
		short res = 0;
		for (int i = 0; i < b.length; i++) {
			res += (b[i] & 0xff) << (i * 8);
		}
		return res;
	}
	
	
	public static short toIntH(byte[] b){
		short res = 0;
	    for(int i=0;i<b.length;i++){
	        res += (b[i] & 0xff) << ((3-i)*8);
	    }
	    return res;
	}
	
	public static List<Integer> toList(String s) {
		if (s.isBlank())
			return null;
		String[] tmp = s.split(",");
		List<Integer> arr = new ArrayList<Integer>();
		for (String k : tmp) {
			try {
				Integer m = Integer.parseInt(k);
				arr.add(m);
			} catch (NumberFormatException ex) {

			}
		} 
		return arr;
	}
	
	/**
	  * 获取某目录下的所有文件
	 * @param directoryPath
	 * @return
	 */
	public static List<String> getAllFile(String directoryPath ) {
        List<String> list = new ArrayList<String>();
        File baseFile = new File(directoryPath);
        if (baseFile.isFile() || !baseFile.exists()) {
            return list;
        }
        File[] files = baseFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()==false) {  
                list.add(file.getAbsolutePath());
            }
        }
        return list;
    } 
}
