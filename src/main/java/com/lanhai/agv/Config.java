package com.lanhai.agv;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser; 

/**
 * 常用配置参数
 * 
 * @author virus408
 *
 */
public class Config {
	//缓存数据文件夹
	public final static String localDBRoot="d:\\agv\\cache";
	
	
	// 小车侦听端口
	private int AgvListenerPort;

	// 对接设备侦听端口
	private List<Integer> StationListenerPort;    

	private int HackListenerPort;

	// 第三方心跳ip
	private String ThirdJumpIP;
	private int ThirdJumpPort;
	private int ThirdJumpLitenerPort;
	
	
	// AGV消息处理线程数
	private int AgvEventThreadNum = 10;
	// 对接设备消息处理线程数
	private int StationEventThreadNum = 10;
	// 内部调度处理线程数
	private int DispatchEventThreadNum;

	// 消息发送线程数
	private int MsgSendThreadNum;

	private String connStr; 
	private String connUser;
	private String connPwd;
	
	private String logStr; 
	private String logUser;
	private String logPwd;
	
	
	public Config() {
		AgvListenerPort = 5000;
		HackListenerPort = 5002;
		StationListenerPort =new ArrayList<Integer>();
		//new int[] { 9700, 9701, 9702 };
		ThirdJumpLitenerPort=8190;
	

		AgvEventThreadNum = 10;
		StationEventThreadNum = 10;
		DispatchEventThreadNum = 2; 
		MsgSendThreadNum = 2;

		connStr = "jdbc:mysql://10.23.101.246:3306/fn_agv_test?serverTimezone=UTC";
		connUser="root";
		connPwd="12345678";
		
		//ThirdJumpIP = "10.247.42.11";
		ThirdJumpIP = "10.23.101.92";
		ThirdJumpPort=9801;
	}

	public int getAgvListenerPort() {
		return AgvListenerPort;
	}

	public void setAgvListenerPort(int agvListenerPort) {
		AgvListenerPort = agvListenerPort;
	}

 

	public List<Integer> getStationListenerPort() {
		return StationListenerPort;
	}

	public void setStationListenerPort(List<Integer> stationListenerPort) {
		StationListenerPort = stationListenerPort;
	}

	public String getThirdJumpIP() {
		return ThirdJumpIP;
	}

	public void setThirdJumpIP(String thirdJumpIP) {
		ThirdJumpIP = thirdJumpIP;
	}

	public int getAgvEventThreadNum() {
		return AgvEventThreadNum;
	}

	public void setAgvEventThreadNum(int agvEventThreadNum) {
		AgvEventThreadNum = agvEventThreadNum;
	}

	public int getStationEventThreadNum() {
		return StationEventThreadNum;
	}

	public void setStationEventThreadNum(int stationEventThreadNum) {
		StationEventThreadNum = stationEventThreadNum;
	}

	public int getDispatchEventThreadNum() {
		return DispatchEventThreadNum;
	}

	public void setDispatchEventThreadNum(int dispatchEventThreadNum) {
		DispatchEventThreadNum = dispatchEventThreadNum;
	}

	public int getMsgSendThreadNum() {
		return MsgSendThreadNum;
	}

	public void setMsgSendThreadNum(int msgSendThreadNum) {
		MsgSendThreadNum = msgSendThreadNum;
	}

	public int getHackListenerPort() {
		return HackListenerPort;
	}

	public void setHackListenerPort(int hackListenerPort) {
		HackListenerPort = hackListenerPort;
	}

	public String getConnStr() {
		return connStr;
	}

	public void setConnStr(String connStr) {
		this.connStr = connStr;
	}

	public String getConnUser() {
		return connUser;
	}

	public void setConnUser(String connUser) {
		this.connUser = connUser;
	}

	public String getConnPwd() {
		return connPwd;
	}

	public void setConnPwd(String connPwd) {
		this.connPwd = connPwd;
	}

	public int getThirdJumpPort() {
		return ThirdJumpPort;
	}

	public void setThirdJumpPort(int thirdJumpPort) {
		ThirdJumpPort = thirdJumpPort;
	}

	public int getThirdJumpLitenerPort() {
		return ThirdJumpLitenerPort;
	}

	public void setThirdJumpLitenerPort(int thirdJumpLitenerPort) {
		ThirdJumpLitenerPort = thirdJumpLitenerPort;
	}

	public String getLogStr() {
		return logStr;
	}

	public void setLogStr(String logStr) {
		this.logStr = logStr;
	}

	public String getLogUser() {
		return logUser;
	}

	public void setLogUser(String logUser) {
		this.logUser = logUser;
	}

	public String getLogPwd() {
		return logPwd;
	}

	public void setLogPwd(String logPwd) {
		this.logPwd = logPwd;
	}

	/**
	 * 加载配置文件 D:\\agv\\agv.conf
	 * 
	 * @throws Exception
	 */
	public void loadConf() throws Exception {
		String filepath="d:\\agv\\agv.conf";
		 FileInputStream fs=null;
		 InputStreamReader isr=null;
		 BufferedReader reader=null;
		 String json="";
		 String line="";
		 try {
			 fs=new FileInputStream(filepath);
			 isr=new InputStreamReader(fs);
			 reader=new BufferedReader(isr); 
			 while( (line=reader.readLine())!=null ) {
				 json+=line;
			 } 
		 }catch(Exception ex) {
			 throw ex;
		 }finally {
			 if(reader!=null) {
				 reader.close();
			 }
			 if(isr!=null) {
				 isr.close();
			 }
			 if(fs!=null) {
				 fs.close();
			 }
		 }
		 if(json.isBlank()) throw new Exception();
		
		 //解析json 
		 JsonElement element = JsonParser.parseString(json);
		 JsonObject root = element.getAsJsonObject();
		 //listener 节点
		 JsonObject listener =root.getAsJsonObject("listener"); 
		 JsonArray stationarr = listener.getAsJsonArray("station");
		 if(stationarr!=null ) {
			 for(int i=0;i<stationarr.size();i++) {
				 int p=stationarr.get(i).getAsInt();
				 StationListenerPort.add(p);
			 }
		 }
		 AgvListenerPort=listener.getAsJsonPrimitive("agv").getAsInt();
		 HackListenerPort= listener.getAsJsonPrimitive("hack").getAsInt();
		 ThirdJumpLitenerPort=listener.getAsJsonPrimitive("heart").getAsInt();
		 
		 //jdbc
		 JsonObject jdbc =root.getAsJsonObject("jdbc"); 
		 String jip=jdbc.getAsJsonPrimitive("ip").getAsString();
		 int jport=jdbc.getAsJsonPrimitive("port").getAsInt();
		 String jdb=jdbc.getAsJsonPrimitive("database").getAsString();
		 connUser=jdbc.getAsJsonPrimitive("user").getAsString();
		 connPwd=jdbc.getAsJsonPrimitive("pwd").getAsString();
		 connStr=String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", jip,jport,jdb);
	
		 //thread
		 JsonObject thd =root.getAsJsonObject("thread"); 
		 AgvEventThreadNum=thd.getAsJsonPrimitive("agv").getAsInt();
		 StationEventThreadNum=thd.getAsJsonPrimitive("station").getAsInt();
		 DispatchEventThreadNum=thd.getAsJsonPrimitive("dispatch").getAsInt();
		 MsgSendThreadNum=thd.getAsJsonPrimitive("push").getAsInt();
		 
		 //heart
		 JsonObject hrt =root.getAsJsonObject("heart");  
		 ThirdJumpIP=hrt.getAsJsonPrimitive("ip").getAsString();
		 ThirdJumpPort=hrt.getAsJsonPrimitive("port").getAsInt();
		 
		 //log
		 JsonObject lg =root.getAsJsonObject("log"); 
		 String lip=lg.getAsJsonPrimitive("ip").getAsString();
		 int lport=lg.getAsJsonPrimitive("port").getAsInt();
		 String ldb=lg.getAsJsonPrimitive("database").getAsString();
		 logUser=lg.getAsJsonPrimitive("user").getAsString();
		 logPwd=lg.getAsJsonPrimitive("pwd").getAsString();
		 logStr=String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", lip,lport,ldb);
	
	}


}
