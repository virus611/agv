package com.lanhai.agv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.lanhai.agv.cache.AgvCache;
import com.lanhai.agv.cache.AgvItem;
import com.lanhai.agv.cache.ParkCache;
import com.lanhai.agv.cache.ParkItem;
import com.lanhai.agv.cache.ParkRelationCache;
import com.lanhai.agv.cache.ParkRelationItem;
import com.lanhai.agv.cache.ParkTypeEnum;
import com.lanhai.agv.cache.ReadyDotCache;
import com.lanhai.agv.cache.ReadyDotItem;
import com.lanhai.agv.cache.RoadCache;
import com.lanhai.agv.cache.RoadItem;
import com.lanhai.agv.cache.EstimateCache;
import com.lanhai.agv.cache.MountCache;
import com.lanhai.agv.cache.StationGroupCache;
import com.lanhai.agv.cache.StationGroupItem;
import com.lanhai.agv.cache.StationGroupSub;
import com.lanhai.agv.cache.TranfficArea;
import com.lanhai.agv.cache.TranfficAreaCache;
import com.lanhai.agv.localstore.AgvMountLocalBean;
import com.lanhai.agv.localstore.AgvMountTools;
import com.lanhai.agv.localstore.AgvLocalBean;
import com.lanhai.agv.localstore.AgvTools;
import com.lanhai.agv.localstore.ParkLocalBean;
import com.lanhai.agv.localstore.ParkTools;
import com.lanhai.agv.localstore.ReadyDotTools;
import com.lanhai.agv.localstore.EstimateLocalBean;
import com.lanhai.agv.localstore.EstimateTools;
import com.lanhai.agv.localstore.TranfficAreaLocalBean;
import com.lanhai.agv.localstore.TranfficAreaTools;
import com.lanhai.agv.thirdpart.ApplyTransferEnum;
import com.lanhai.agv.thirdpart.StationInterface;
import com.lanhai.agv.thirdpart.StationResolveFactory; 

public class Main {

	public static void main111(String[] args) throws Exception {
		System.out.println("程序启动...." );
		Config cnf=new Config();
		
		
		System.out.println("加载配置,路径 D:\\agv\\agv.conf...." ); 
		try {
			cnf.loadConf();
		}catch(Exception e) {
			e.printStackTrace(); 
			System.out.println("加载配置出错，程序将自动退出" );
			System.exit(0); 
		}
		
		
		System.out.println("加载数据中...." ); 
		try {
			 loadDbData(cnf);
		}catch(Exception e) {
			e.printStackTrace(); 
			System.out.println("加载数据出错，程序将自动退出" );
			System.exit(0); 
		}
		
		
		
		
		
		DispatchCore core=new DispatchCore(cnf);
		core.Start();
		 
		System.out.println("程序启动成功，输入大写C退出" );
		while(System.in.read()!=67) {
			
		}
		core.Stop();
		System.out.println("程序正常结束" );
		System.exit(0);
	}
	
	public static void main (String[] args) throws Exception {
		Map<String,String> map=new HashMap<String,String>();
		map.put("DR", "蒂尔");
		map.put("JS_Q", "江松-钱");
		map.put("JS_W", "江松-吴");
		map.put("JS_Y", "江松-俞");
		map.put("MW", "迈为");
		map.put("ROBO", "罗博特科");
		map.put("WD", "微导");
		map.put("ZDES", "中电二所");
		map.put("LH", "发料机");
		 
		for(Entry<String,String> entry:map.entrySet()) {
			StationInterface st=StationResolveFactory.getStationResolve(entry.getKey());
			if(st==null) continue;
			
			System.out.println(entry.getValue());
			System.out.println("申请上料");
			System.out.println(Tools.byte2HexStr(st.applyTransfer(ApplyTransferEnum.In)));
		 
			
			System.out.println("申请下料");
			System.out.println(Tools.byte2HexStr(st.applyTransfer(ApplyTransferEnum.Out)));
	 
			
			System.out.println("同时传输");
			System.out.println(Tools.byte2HexStr(st.applyTransfer(ApplyTransferEnum.Both)));
 
			
			System.out.println("申请离开");
			System.out.println(Tools.byte2HexStr(st.applyLeave()));
		 
			System.out.println("清零");
			System.out.println(Tools.byte2HexStr(st.clear()));
			System.out.println("");
			
		}
		
	}
	
	
	
	
	
	
	/**
	 * 加载数据库数据
	 * 
	 * @throws Exception
	 */
	 static void loadDbData(Config cnf) throws Exception {
		 DBHelper helper=new DBHelper(cnf.getConnStr(),cnf.getConnUser(),cnf.getConnPwd());
		////  路线
		 List<RoadItem> roadList=helper.getRoad();
		 if(roadList.size()>0) {
			 //路线和工序的映射
			 for(RoadItem road:roadList) {
				 
					 RoadCache.add(road.getRoadId(),  road.getLv());
					  
			 }
		 } 
		
		////  加载小车数据及缓存
		List<AgvItem> agvList=helper.getAgvList();
		List<AgvLocalBean> agvbeanList=AgvTools.loadFile();
		if(agvbeanList.size()>0) {
			for(AgvItem t1:agvList) {
				for(AgvLocalBean t2:agvbeanList) {
					if(t1.getAgvId()==t2.getAgvId()) {
						t1.loadDB(t2); 
						break;
					}
				}
			}
		}
		AgvCache.addList(agvList);
	 
		
		
		//// 加载预判断点野车列表 
		List<EstimateLocalBean> roadbeanList=EstimateTools.loadFile();
		if(roadbeanList.size()>0) {
			for( EstimateLocalBean rc:roadbeanList) {
				EstimateCache.add(rc.getParkNo(),rc.getStopAgv());
			}
		}
	 
		
		//// 加载装卸情况 MountCache 
		List<AgvMountLocalBean> mountList=AgvMountTools.loadFile();
		MountCache.add(mountList); 
	  
		
		//交管
		initTraffic(helper);
		 
		
		//停泊点
		initPark(helper,roadList);
		 
		
	 }
	 
	 
	 
	 
	 
	 
	 /**
	  * 初始化交管区域
	 * @param helper
	 * @throws Exception
	 */
	static void initTraffic(DBHelper helper) throws Exception {
			List<TranfficArea> areaList=helper.getTranfficAreaList();
			List<TranfficAreaLocalBean> areabeanList=TranfficAreaTools.loadFile();
			if(areaList.size()>0) {
				for(TranfficArea t1:areaList) {
					for(TranfficAreaLocalBean t2:areabeanList) {
						if(t1.getAreaId()==t2.getAreaId()) {
							t1.loadDB(t2); 
							break;
						}
					}
				}
			}
			TranfficAreaCache.addList(areaList);
	 }
	 
	 /**
	  * 初始化停泊点及其相关的数据
	 * @param helper
	 * @param roadList
	 * @throws Exception
	 */
	static void initPark( DBHelper helper ,List<RoadItem> roadList) throws Exception {
	////  加载停泊位( 停泊位，left 对接设备）
		 
			List<ParkItem> parkList=helper.getParkList();
			//缓存的对接设备装卸情况
			List<ParkLocalBean> parkbeanList=ParkTools.loadFile();
			if(parkList.size()>0) {
				for(ParkItem t1:parkList) {
					for(ParkLocalBean t2:parkbeanList) {
						if(t1.getParkNo()==t2.getParkNo()) {
							t1.loadDB(t2); 
							break;
						}
					}
				}
			}
			
			//路线点和停泊点之间的关联
			if (parkList.size() > 0 && roadList.size() > 0) { 
				for (RoadItem roaditem : roadList) {
					for (ParkItem parkitem : parkList) {
						if (roaditem.getSourceParkNo() == parkitem.getParkNo()) {
							parkitem.setSourceRoad(roaditem.getRoadId());
						}
						if (roaditem.getTargetParkNo() == parkitem.getParkNo()) {
							parkitem.setTargetRoad(roaditem.getRoadId());
						}
					}
				}
			}
			
			//待命点 ReadyDotCache
			List<ParkItem> rList=new ArrayList<ParkItem>(); //装卸分配点
			List<ReadyDotItem> readyDotlist=new ArrayList<ReadyDotItem>();
			for (ParkItem parkitem : parkList) {
				if(parkitem.getType()==ParkTypeEnum.TransReady) { 
					rList.add(parkitem);
				} else if(parkitem.getType()==ParkTypeEnum.Wait) {
					//创建待命点
					ReadyDotItem ob=new ReadyDotItem();
					ob.setAgvId(0);
					ob.setSourceRoad(parkitem.getSourceRoad());
					ob.setTargetRoad(parkitem.getTargetRoad());
					ob.setLastTime(0); 
					ob.setParkNo(parkitem.getParkNo());
					readyDotlist.add(ob);
				}
			}
			//加载待命点停小车的缓存
			List<ReadyDotItem> r2List=ReadyDotTools.loadFile();
			if(r2List.size()>0) {
				for (ReadyDotItem parkitem : readyDotlist) {
					for (ReadyDotItem r2 : r2List) {
						if(parkitem.getParkNo()==r2.getParkNo() ) {
							parkitem.loadDB(r2);
							break;
						}
					}
				}
			} 
			//关联预判断位和装卸分配点的关系
			List<ParkRelationItem> rr1=helper.getEstimateParkRelaction();
			ParkRelationCache.addEstimate(rr1);
			 
			//关联装卸分配点和释放点的关系
			List<ParkRelationItem> rr2=new ArrayList<ParkRelationItem>();
			for (ParkItem parkitem : rList) {
				
				ParkRelationItem pi=new ParkRelationItem();
				pi.setStartdot(parkitem.getParkNo());
				pi.setEnddot(parkitem.getReleaseDot());
				rr2.add(pi); 
			}
			ParkRelationCache.addReleaseList(rr2);
			 
			
			//重构分配点
			for (ParkItem parkitem : rList) {
				// 分配点：分组工序
				ReadyDotCache.addProcessGroup(parkitem.getParkNo(), parkitem.getProcessGroup()); 
				List<Integer> ar=Tools.toList(parkitem.getReadydotarr());
				List<ReadyDotItem> t1=new ArrayList<ReadyDotItem>();
				if(ar.size()>0) { 
					// 装卸分配点：可用待命点位 
					for(ReadyDotItem k :r2List) {
						 if( ar.contains(k.getParkNo())) {
							 t1.add(k.copy());
						 }
					}
				}
				ReadyDotCache.addReadyDotList(parkitem.getParkNo(),t1);
			}
			 
			//停泊位缓存状态
			if(parkList.size()>0) {
				//加载设备分组
				//StationGroupCache
				List<String> processList=helper.getProcessGroup();
				for(String process: processList) {
					
					List<StationGroupSub> sublist=new ArrayList<StationGroupSub>();
					for(ParkItem item:parkList) {
						if(item.getProcessGroup().equals(process)) {
							StationGroupSub sg=new StationGroupSub();
							sg.setStationGroup(item.getStationGroup());
							sg.setAgvIp("");
							sg.setDisable(item.isStationDisable());
							sg.setLock(false); 
							sublist.add(sg);
						}
					} 
					StationGroupItem sub=new StationGroupItem();
					sub.setList(sublist);
					StationGroupCache.add(process, sub);
				} 
			}  
			ParkCache.addList(parkList);
			
			////  加载停泊位  end
	 }
}