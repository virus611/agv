package com.lanhai.agv.pos;
 
import java.util.List;
 

public class AgvPosThread {
	private boolean running;
	private Thread th;
	private PosDBHelper helper = null;

	public AgvPosThread() {
		try {
			helper = new PosDBHelper();
		} catch (Exception e) {

		}
	}

	public void Start() {
		running = true;
		th = new Thread() {
			@Override
			public void run() {

				while (running) {
					List<AgvPosItem> arr = AgvPosQueue.getAll();

					if (helper != null) {
						helper.logAgv(arr);
					}

					// 写数据库，如果不写库，可以注释掉
					try {
						Thread.sleep(1000);
					} catch (Exception ex) {

					}
				}
			}

		};
		th.start();
	}

	public void Stop() {
		running = false;
		try {
			th.interrupt();
			th = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
