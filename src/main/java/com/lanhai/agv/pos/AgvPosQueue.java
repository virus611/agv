package com.lanhai.agv.pos;

import java.util.ArrayList;
import java.util.List;

/**
 * 记录点位
 * @author virus408
 *
 */
public class AgvPosQueue {
	static List<AgvPosItem> list;
	static {
		list = new ArrayList<AgvPosItem>();
	}

	public static synchronized void Push(int agvid, int x, int y, int lv) {
		AgvPosItem item = null;
		for (AgvPosItem xp : list) {
			if (xp.getAgvid() == agvid) {
				item = xp;
				xp.setX(x);
				xp.setY(y);
				xp.setLv(lv);
				break;
			}
		}
		if (item == null) {
			item = new AgvPosItem();
			item.setAgvid(agvid);
			item.setLv(lv);

			item.setX(x);
			item.setY(y);
			list.add(item);
		}
	}

	public static  List<AgvPosItem> getAll(){
		List<AgvPosItem> ls=new ArrayList<AgvPosItem>();
		for (AgvPosItem xp : list) {
			ls.add(xp.copy());
		}
		return ls;
	}
}
