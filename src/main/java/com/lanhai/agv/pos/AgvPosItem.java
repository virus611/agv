package com.lanhai.agv.pos;

public class AgvPosItem {
	private int agvid;
	private int x;
	private int y;
	private int lv;
	
	public AgvPosItem copy() {
		AgvPosItem obj=new AgvPosItem();
		obj.setAgvid(agvid);
		obj.setX(x);
		obj.setY(y);
		obj.setLv(lv);
		return obj;
	}

	public int getAgvid() {
		return agvid;
	}

	public void setAgvid(int agvid) {
		this.agvid = agvid;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}

}
