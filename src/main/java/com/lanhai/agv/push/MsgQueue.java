package com.lanhai.agv.push;

import java.util.LinkedList;
import java.util.Queue;

import com.lanhai.agv.log.LogData;
import com.lanhai.agv.log.LogQueue;
 

/**
 * 发送的消息队列 
 * @author virus408
 *
 */
public class MsgQueue {
	static Queue<MsgData> queue = new LinkedList<MsgData>();

	public static synchronized void Push(MsgData item) {
		LogData lb=LogData.fromMsgData(item);
		LogQueue.Push(lb);
		queue.offer(item);

	}

	public static synchronized MsgData Pop() {

		if (queue.isEmpty()) {
			return null;
		}
		MsgData item = queue.poll();
		return item;
	}
}
