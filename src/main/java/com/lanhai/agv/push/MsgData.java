package com.lanhai.agv.push;

/**
 * 发送的消息体
 * 
 * @author virus408
 *
 */
public class MsgData {
	
	public MsgData(String ip,int port,byte[] data,String remark ) {
		this.ip=ip;
		this.port=port;
		this.data=data;
		this.remark=remark;
	}
	
	private String ip;
	private int port;
	private byte[] data;
	private String remark;
	private int agvId;
	 
	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
