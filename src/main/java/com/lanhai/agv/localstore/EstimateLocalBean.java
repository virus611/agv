package com.lanhai.agv.localstore;

import java.io.Serializable;
import java.util.List;

/**
 * 预判断点
 * @author virus408
 *
 */
public class EstimateLocalBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 
	private int parkNo; 
	private List<Integer> stopAgv;

	  

	public List<Integer> getStopAgv() {
		return stopAgv;
	}

	public void setStopAgv(List<Integer> stopAgv) {
		this.stopAgv = stopAgv;
	}

	public int getParkNo() {
		return parkNo;
	}

	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}

}
