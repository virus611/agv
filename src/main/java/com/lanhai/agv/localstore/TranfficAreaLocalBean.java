package com.lanhai.agv.localstore;

import java.io.Serializable;
import java.util.List;

/**
 * 本地缓存交管数据
 * 
 * @author virus408
 *
 */
public class TranfficAreaLocalBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private int agvId;
	 
	private int areaId; 
	private List<Integer> stopAgv;

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	 
	public List<Integer> getStopAgv() {
		return stopAgv;
	}

	public void setStopAgv(List<Integer> stopAgv) {
		this.stopAgv = stopAgv;
	}

	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	 
	 
}
