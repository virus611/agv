package com.lanhai.agv.localstore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.Config;
import com.lanhai.agv.Tools;
import com.lanhai.agv.cache.ReadyDotItem;

/**
 * 待命点缓存
 * @author virus408
 *
 */
public class ReadyDotTools {
	public static void saveFile(ReadyDotItem obj) {
		String filename = String.format("%s\\waitdot\\%d.data", Config.localDBRoot, obj.getParkNo());
		try {
			File ff = new File(filename);
			FileOutputStream fos = new FileOutputStream(ff);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<ReadyDotItem> loadFile() {
		String filepath = String.format("%s\\waitdot\\", Config.localDBRoot);
		List<ReadyDotItem> arr = new ArrayList<ReadyDotItem>();
		List<String> filelist = Tools.getAllFile(filepath);
		if(filelist==null||filelist.size()==0) return arr;
		
		for (String filename : filelist) {
			try {
				FileInputStream fileIn = new FileInputStream(filename);
				ObjectInputStream inputReader = new ObjectInputStream(fileIn);
				ReadyDotItem obj = (ReadyDotItem) inputReader.readObject(); 
				inputReader.close();
				arr.add(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
}

