package com.lanhai.agv.localstore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.Config;
import com.lanhai.agv.Tools;

public class AgvTools {
	public static void saveFile(AgvLocalBean obj) {
		String filename = String.format("%s\\agv\\%d.data", Config.localDBRoot, obj.getAgvId());
		try {
			File ff = new File(filename);
			FileOutputStream fos = new FileOutputStream(ff);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<AgvLocalBean> loadFile() {
		String filepath = String.format("%s\\agv\\", Config.localDBRoot);
		List<AgvLocalBean> arr = new ArrayList<AgvLocalBean>();
		List<String> filelist = Tools.getAllFile(filepath);
		if(filelist==null||filelist.size()==0) return arr;
		
		for (String filename : filelist) {
			try {
				FileInputStream fileIn = new FileInputStream(filename);
				ObjectInputStream inputReader = new ObjectInputStream(fileIn);
				AgvLocalBean obj = (AgvLocalBean) inputReader.readObject(); 
				inputReader.close();
				arr.add(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
}
