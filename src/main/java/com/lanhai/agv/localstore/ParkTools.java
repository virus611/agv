package com.lanhai.agv.localstore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.Config;
import com.lanhai.agv.Tools;

/**
 * 停泊位
 * @author virus408
 *
 */
public class ParkTools {
	public static void saveFile(ParkLocalBean obj) {
		String filename = String.format("%s\\park\\%d.data", Config.localDBRoot, obj.getParkNo());
		try {
			File ff = new File(filename);
			FileOutputStream fos = new FileOutputStream(ff);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<ParkLocalBean> loadFile() {
		String filepath = String.format("%s\\park\\", Config.localDBRoot);
		List<ParkLocalBean> arr = new ArrayList<ParkLocalBean>();
		List<String> filelist = Tools.getAllFile(filepath);
		if(filelist==null||filelist.size()==0) return arr;
		
		for (String filename : filelist) {
			try {
				FileInputStream fileIn = new FileInputStream(filename);
				ObjectInputStream inputReader = new ObjectInputStream(fileIn);
				ParkLocalBean obj = (ParkLocalBean) inputReader.readObject(); 
				inputReader.close();
				arr.add(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
}
