package com.lanhai.agv.localstore;

import java.io.Serializable;

/**
 * 停泊点信息
 * @author virus408
 *
 */
public class ParkLocalBean implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//需要卸料
	private boolean needOut;
	//需要上料
	private boolean needIn;
	//现有花篮数量
	private int transInNum;
	private int transOutNum;
	//传输中
	private boolean transing;
	  
	 
	//小车 
	private String agvIP; 
	//最后传输时间
	private long  lastTime;
	
	private int parkNo;
	
	
	public int getParkNo() {
		return parkNo;
	}
	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}
	public boolean isNeedOut() {
		return needOut;
	}
	public void setNeedOut(boolean needOut) {
		this.needOut = needOut;
	}
	public boolean isNeedIn() {
		return needIn;
	}
	public void setNeedIn(boolean needIn) {
		this.needIn = needIn;
	}
	public int getTransInNum() {
		return transInNum;
	}
	public void setTransInNum(int transInNum) {
		this.transInNum = transInNum;
	}
	public int getTransOutNum() {
		return transOutNum;
	}
	public void setTransOutNum(int transOutNum) {
		this.transOutNum = transOutNum;
	}
	public boolean isTransing() {
		return transing;
	}
	public void setTransing(boolean transing) {
		this.transing = transing;
	}
	 
	public String getAgvIP() {
		return agvIP;
	}
	public void setAgvIP(String agvIP) {
		this.agvIP = agvIP;
	}
	public long getLastTime() {
		return lastTime;
	}
	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}
	
	
}
