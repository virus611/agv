package com.lanhai.agv.localstore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.Config;
import com.lanhai.agv.Tools;

public class EstimateTools {

	public static void saveFile(EstimateLocalBean obj) {
		String filename = String.format("%s\\estimate\\%d.data", Config.localDBRoot, obj.getParkNo());
		try {
			File ff = new File(filename);
			FileOutputStream fos = new FileOutputStream(ff);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<EstimateLocalBean> loadFile() {
		String filepath = String.format("%s\\estimate\\", Config.localDBRoot);
		List<EstimateLocalBean> arr = new ArrayList<EstimateLocalBean>();
		List<String> filelist = Tools.getAllFile(filepath);
		if(filelist==null||filelist.size()==0) return arr;
		
		for (String filename : filelist) {
			try {
				FileInputStream fileIn = new FileInputStream(filename);
				ObjectInputStream inputReader = new ObjectInputStream(fileIn);
				EstimateLocalBean obj = (EstimateLocalBean) inputReader.readObject(); 
				inputReader.close();
				arr.add(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
}
