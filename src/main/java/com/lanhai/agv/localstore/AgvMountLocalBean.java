package com.lanhai.agv.localstore;

import java.io.Serializable;

/**
 * 小车装载信息
 * 
 * @author virus408
 *
 */
public class AgvMountLocalBean  implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int agvId;
	private String agvIp;
	private boolean upFull;
	private boolean downFull;
	private boolean finishSend;
	private boolean finishReceive;

  

	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	public boolean isUpFull() {
		return upFull;
	}

	public void setUpFull(boolean upFull) {
		this.upFull = upFull;
	}

	public boolean isDownFull() {
		return downFull;
	}

	public void setDownFull(boolean downFull) {
		this.downFull = downFull;
	}

	public boolean isFinishSend() {
		return finishSend;
	}

	public void setFinishSend(boolean finishSend) {
		this.finishSend = finishSend;
	}

	public boolean isFinishReceive() {
		return finishReceive;
	}

	public void setFinishReceive(boolean finishReceive) {
		this.finishReceive = finishReceive;
	}

	public String getAgvIp() {
		return agvIp;
	}

	public void setAgvIp(String agvIp) {
		this.agvIp = agvIp;
	}

}
