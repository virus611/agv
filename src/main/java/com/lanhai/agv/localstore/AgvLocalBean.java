package com.lanhai.agv.localstore;

import java.io.Serializable;

/**
 * 小车临时数据
 * 
 * @author virus408
 *
 */
public class AgvLocalBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int currentX;
	private int currentY;

	private boolean online;

	private int areaId;
	private boolean moving;

	private int parkNo;
	private int roadid;

	private int agvId;
	// 目标工序分组
	private String nextProcess;

	// 充电次数
	private int powerTime;

	public int getPowerTime() {
		return powerTime;
	}

	public void setPowerTime(int powerTime) {
		this.powerTime = powerTime;
	}

	public String getNextProcess() {
		return nextProcess;
	}

	public void setNextProcess(String nextProcess) {
		this.nextProcess = nextProcess;
	}

	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	public int getCurrentX() {
		return currentX;
	}

	public void setCurrentX(int currentX) {
		this.currentX = currentX;
	}

	public int getCurrentY() {
		return currentY;
	}

	public void setCurrentY(int currentY) {
		this.currentY = currentY;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public int getParkNo() {
		return parkNo;
	}

	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}

	public int getRoadid() {
		return roadid;
	}

	public void setRoadid(int roadid) {
		this.roadid = roadid;
	}

}
