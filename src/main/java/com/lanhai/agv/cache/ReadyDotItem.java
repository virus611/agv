package com.lanhai.agv.cache;

import java.io.Serializable;
 

/**
 * 待命点
 * 
 * @author virus408
 *
 */
public class ReadyDotItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 最后更新时间
	private long lastTime;
	// 回退的路线
	private int targetRoad;
	// 进去的路线
	private int sourceRoad;
	

	// 点位
	private int parkNo;
 

	// 关联的工序分组名（与小车相同）
	private String processName;
	// 停靠的小车id
		private int agvId = 0;

	public ReadyDotItem copy() {
		ReadyDotItem obj = new ReadyDotItem();
		obj.setAgvId(agvId);
		obj.setLastTime(lastTime);
		obj.setParkNo(parkNo);
		obj.setSourceRoad(sourceRoad);
		obj.setTargetRoad(targetRoad); 
		obj.setProcessName(processName);
		return obj;
	}

	public void loadDB(ReadyDotItem obj) {
		agvId = obj.getAgvId();
		lastTime = obj.getLastTime();
		processName = obj.getProcessName();
	}
 
	public long getLastTime() {
		return lastTime;
	}

	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	public int getParkNo() {
		return parkNo;
	}

	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}

	public int getTargetRoad() {
		return targetRoad;
	}

	public void setTargetRoad(int targetRoad) {
		this.targetRoad = targetRoad;
	}

	public int getSourceRoad() {
		return sourceRoad;
	}

	public void setSourceRoad(int sourceRoad) {
		this.sourceRoad = sourceRoad;
	}

}
