package com.lanhai.agv.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map; 

/**
 * 工序分组
 * @author virus408
 *
 */
/**
 * @author virus408
 *
 */
public class StationGroupCache {
	//工序分组：工序集合
	static Map<String, StationGroupItem> map;
	
	//传输工位：装卸分配点位
	static Map<Integer, Integer> readymap;
	
	//小车ip,工序分组 agvip,processName   
	//外部不可访问，用于快速查找
	 static Map<String,String> lockAgv;

	static {
		map = new HashMap<String, StationGroupItem>();
		lockAgv=new HashMap<String, String>();
		readymap=new HashMap<Integer,Integer>();
	}
	
	/**
	 * 添加工序分组和对应的设备分组
	 * @param processName
	 * @param item
	 */
	public static void add(String processName,StationGroupItem item) {
		map.put(processName, item);
	}
	
	
	/**
	 * 更新设备分组的可用状态
	 * @param processName
	 * @param groupName
	 * @param Disable
	 */
	public static void updateDisable(String processName,String groupName,boolean Disable) {
		/*
		for(Entry<String ,StationGroupItem> entry : map.entrySet()) {
			entry.getValue().updateDisable(groupName, Disable);
		}
		*/
		StationGroupItem it=map.get(processName);
		if(it!=null) {
			it.updateDisable(groupName, Disable);
			map.put(processName, it);
		}
	}
	
	
	/**
	 * 获取指定工序分组对应的可用的设备分组名称
	 * 一个工序分组存在多个设备分组名称（比如L1,L2,L3）
	 * @param processName 工序分组
	 * @return 设备分组名称
	 */
	public static List<String> getCanUseStationGroup(String processName){
		StationGroupItem it=map.get(processName);
		if(it==null) return null;
		return it.getCanUseNames();
	} 
	
	
	/**
	 * 加锁或者解锁设备
	 * @param processName
	 * @param groupName
	 * @param agvIp
	 * @param lockflag
	 */
	public  static synchronized void lockGroup(String processName,String groupName,String agvIp) {
		StationGroupItem it=map.get(processName);
		if(it==null) return  ;
		
		lockAgv.put(agvIp, processName);
		it.lockGroup(groupName,agvIp); 
	}
	
	/**
	 * 获取小车锁定的工序分组
	 * @param agvIp
	 * @return
	 */
	public static String getProcessNameByAgvIp(String agvIp) {
		String pro=lockAgv.get(agvIp);
		return pro;
	}
	
	/**
	 * 通过小车IP解锁设备分组，用于资源释放
	 * @param agvIp
	 */
	public static synchronized  void unlockForAgv( String agvIp) {
		String processName=lockAgv.get(agvIp);
		if(processName==null) return ;
		
		lockAgv.remove(agvIp);
		
		StationGroupItem sg=map.get(processName);
		sg.unlockForAgv(agvIp); 
	} 
	
	 
	
	 /**
	  * 新增传输工位和分配点的关联关系
	 * @param transno
	 * @param readyno
	 */
	public static void addreadySource(int transno,int readyno) {
		 readymap.put(transno, readyno);
	 }


	/**
	 * 获取传输位对应的预分配点
	 * @param transno
	 * @return
	 */
	public static int getReadySource(int transno) {
		Integer p=readymap.get(transno);
		if(p==null) return 0;
		return p;
	}
	 
}
