package com.lanhai.agv.cache;
  
import com.lanhai.agv.localstore.ParkLocalBean; 

/**
 * 停泊点
 * @author virus408
 *
 */
public class ParkItem {
	private int parkNo;
	private ParkTypeEnum type;
	 
	//区域
	private int MinX;
	private int MaxX;
	private int MinY;
	private int MaxY;
	
	//对接设备上下料类型（只上，只下，都有）
	private ParkTransPosEnum transType;
	private String stationIP; 
	private boolean stationDisable;
	private int stationPort;
	private String cmdSign;
	
	
	//需要卸料
	private boolean needOut;
	//需要上料
	private boolean needIn;
	//现有花篮数量
	private int transInNum;
	private int transOutNum;
	//传输中
	private boolean transing;
	
	//来源路线（设备分组用）
	private int sourceRoad;
	private int targetRoad;

	 //设备分组权重
	private int weight;
	
	//上层可传输
	private boolean stationUpTrans;
	//下层可传输
	private boolean stationDownTrans;
	
	
	//小车 
	private String agvIP;
	
	//设备分组
	private String stationGroup;
	//工序分组
	private String processGroup;

	//最后传输时间
	private long  lastTime;
	
	//选择设备分组策略类型
	/*
	 * 0=按权重分配  1=按数量分配
	 */
	private int selectType;
	 
	
	//数据表中的字段，仅初始化使用
	private String  readydotarr;
	
	//装卸释放点专用字段，对应的装卸分配点
	private int releaseDot;
	
	
	//关联的预判断点位，初始时需要做运算
	private int estimate;
	
	//所在区段
	private int lv;
	
	private String nextprocess;
	
	private String tag;
	public ParkItem( ) {
		 
		stationIP="";
		stationUpTrans=false;
		stationDownTrans=false;
		weight=0;
		cmdSign="";
		stationGroup="";
		processGroup="";
		agvIP="";
		estimate=0;
	}
	
	public void loadDB( ParkLocalBean obj) {
		needOut=obj.isNeedOut();
		needIn=obj.isNeedIn();
		transInNum=obj.getTransInNum();
		transOutNum=obj.getTransOutNum();
		transing=obj.isTransing();
		agvIP=obj.getAgvIP();
		lastTime=obj.getLastTime();
	}
	
	public ParkLocalBean toBean() {
		ParkLocalBean obj=new ParkLocalBean();
		 obj.setAgvIP(agvIP);
		 obj.setParkNo(parkNo);
		 obj.setNeedIn(needIn);
		 obj.setNeedOut(needOut);
		 obj.setTransInNum(transInNum);
		 obj.setTransOutNum(transOutNum);
		 obj.setTransing(transing);
		 obj.setLastTime(lastTime);
		return obj;
	}
	
	public void setArea(int x,int y) {
		int r=50; //50cm
		MinX=x-r;
		MaxX=x+r;
		MinY=y-r;
		MaxY=y+r;
	}
	
	public boolean inArea(int x,int y) {
		if(MinX<=x&&x<=MaxX
				&&MinY<=y&&y<=MaxY) {
			return true;
		}else {
			return false;
		}
	}
	
	 
	public  ParkItem  copy() {
		ParkItem obj=new ParkItem( );
		obj.setAgvIP(agvIP);
		obj.setTransType(transType); 
		obj.setTargetRoad(targetRoad); 
		obj.setParkNo(parkNo);
		obj.setType(type);
		obj.setStationIP(stationIP);
		obj.setStationPort(stationPort); 	
		obj.setStationGroup(stationGroup);
		obj.setSourceRoad(sourceRoad);
		obj.setProcessGroup(processGroup);
		obj.setNeedIn(needIn);
		obj.setNeedOut(needOut);
		obj.setWeight(weight); 
		obj.setTransInNum(transInNum);
		obj.setTransOutNum(transOutNum);
		obj.setSelectType(selectType);
		obj.setStationDisable(stationDisable); 
		obj.setReadydotarr(readydotarr);
		obj.setReleaseDot(releaseDot);
		obj.setEstimate(estimate);
		obj.setLv(lv);
		obj.setNextprocess(nextprocess);
		obj.setTag(tag);
		return obj;
	}
	
	 

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getNextprocess() {
		return nextprocess;
	}

	public void setNextprocess(String nextprocess) {
		this.nextprocess = nextprocess;
	}

	public int getEstimate() {
		return estimate;
	}

	public void setEstimate(int estimate) {
		this.estimate = estimate;
	}

	public int getReleaseDot() {
		return releaseDot;
	}

	public void setReleaseDot(int releaseDot) {
		this.releaseDot = releaseDot;
	}

  
	public String getStationGroup() {
		return stationGroup;
	}


	public void setStationGroup(String stationGroup) {
		this.stationGroup = stationGroup;
	}


	public int getParkNo() {
		return parkNo;
	}


	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}


	public ParkTypeEnum getType() {
		return type;
	}


	public void setType(ParkTypeEnum type) {
		this.type = type;
	}

 

	public int getMinX() {
		return MinX;
	}


	public void setMinX(int minX) {
		MinX = minX;
	}


	public int getMaxX() {
		return MaxX;
	}


	public void setMaxX(int maxX) {
		MaxX = maxX;
	}


	public boolean isStationDisable() {
		return stationDisable;
	}

	public void setStationDisable(boolean stationDisable) {
		this.stationDisable = stationDisable;
	}

	public int getMinY() {
		return MinY;
	}


	public void setMinY(int minY) {
		MinY = minY;
	}


	public int getMaxY() {
		return MaxY;
	}


	public void setMaxY(int maxY) {
		MaxY = maxY;
	}


	public ParkTransPosEnum getTransType() {
		return transType;
	}


	public void setTransType(ParkTransPosEnum transType) {
		this.transType = transType;
	}


	public String getStationIP() {
		return stationIP;
	}


	public void setStationIP(String stationIP) {
		this.stationIP = stationIP;
	}


	public int getStationPort() {
		return stationPort;
	}


	public void setStationPort(int stationPort) {
		this.stationPort = stationPort;
	}


	public int getSourceRoad() {
		return sourceRoad;
	}


	public void setSourceRoad(int sourceRoad) {
		this.sourceRoad = sourceRoad;
	}


	public boolean isNeedOut() {
		return needOut;
	}


	public void setNeedOut(boolean needOut) {
		this.needOut = needOut;
	}


	public boolean isNeedIn() {
		return needIn;
	}


	public void setNeedIn(boolean needIn) {
		this.needIn = needIn;
	}


	public boolean isStationUpTrans() {
		return stationUpTrans;
	}


	public void setStationUpTrans(boolean stationUpTrans) {
		this.stationUpTrans = stationUpTrans;
	}


	public boolean isStationDownTrans() {
		return stationDownTrans;
	}


	public void setStationDownTrans(boolean stationDownTrans) {
		this.stationDownTrans = stationDownTrans;
	}

 

	public String getAgvIP() {
		return agvIP;
	}


	public void setAgvIP(String agvIP) {
		this.agvIP = agvIP;
	}


	public String getCmdSign() {
		return cmdSign;
	}


	public void setCmdSign(String cmdSign) {
		this.cmdSign = cmdSign;
	}


	public String getProcessGroup() {
		return processGroup;
	}


	public void setProcessGroup(String processGroup) {
		this.processGroup = processGroup;
	}

 


	public int getTransInNum() {
		return transInNum;
	}

	public void setTransInNum(int transInNum) {
		this.transInNum = transInNum;
	}

	public int getTransOutNum() {
		return transOutNum;
	}

	public void setTransOutNum(int transOutNum) {
		this.transOutNum = transOutNum;
	}

	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}


	public int getSelectType() {
		return selectType;
	}


	public void setSelectType(int selectType) {
		this.selectType = selectType;
	}


	public long getLastTime() {
		return lastTime;
	}


	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

	public boolean isTransing() {
		return transing;
	}

	public void setTransing(boolean transing) {
		this.transing = transing;
	}

	public int getTargetRoad() {
		return targetRoad;
	}

	public void setTargetRoad(int targetRoad) {
		this.targetRoad = targetRoad;
	}

	public String getReadydotarr() {
		return readydotarr;
	}

	public void setReadydotarr(String readydotarr) {
		this.readydotarr = readydotarr;
	}

	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}
	
	
	
	
	
}
