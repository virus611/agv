package com.lanhai.agv.cache;

/**
 * 路线
 * 
 * @author virus408
 *
 */
public class RoadItem {
	//路线id
	private int roadId;
	//对应停泊点
	private int sourceParkNo;
	
	//目标停泊位
	private int targetParkNo;
	 
	 
	
	//区段
	private int lv;

	public int getRoadId() {
		return roadId;
	}

	public void setRoadId(int roadId) {
		this.roadId = roadId;
	}

	 
	public int getSourceParkNo() {
		return sourceParkNo;
	}

	public void setSourceParkNo(int sourceParkNo) {
		this.sourceParkNo = sourceParkNo;
	}

	public int getTargetParkNo() {
		return targetParkNo;
	}

	public void setTargetParkNo(int targetParkNo) {
		this.targetParkNo = targetParkNo;
	}

 

	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}

	 

}
