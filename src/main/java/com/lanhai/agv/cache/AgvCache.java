package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.dispatch.AgvCmd;
import com.lanhai.agv.localstore.AgvTools;
import com.lanhai.agv.push.MsgData;
import com.lanhai.agv.push.MsgQueue; 

public class AgvCache {
	static List<AgvItem> list;

	static {
		list = new ArrayList<AgvItem>();
	}

	public static synchronized void addList(List<AgvItem> ls) {
		if(ls!=null&&ls.size()>0) list.addAll(ls);
	}

	/**
	 * 通过ip查找小车
	 * 
	 * @param ip
	 * @return
	 */
	public static synchronized AgvItem getAgvByIP(String ip) {
		AgvItem p = null;
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				p = item.copy();
				break;
			}
		}
		return p;
	}

	/**
	 * 通过id查小车
	 * @param id
	 * @return
	 */
	public static synchronized AgvItem getAgvByID(int id) {
		AgvItem p = null;
		for (AgvItem item : list) {
			if (item.getAgvId()==id) {
				p = item.copy();
				break;
			}
		}
		return p;
	}
	
	
	/**
	 * 查找停泊点的小车
	 * 
	 * @param parkNo
	 * @return
	 */
	public static synchronized AgvItem getAgvByPark(int parkNo ) {
		AgvItem p = null;
		for (AgvItem item : list) {
			if (item.getParkNo() == parkNo ) {
				p = item.copy();
				break;
			}
		}
		return p;
	}

	/**
	 * 移动中设置小车状态
	 * 
	 * @param ip
	 * @param x
	 * @param y
	 */
	public static synchronized void updateAgvInfo(String ip, int x, int y,int roadid, boolean stop) {
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				item.setCurrentX(x);
				item.setCurrentY(y);
				if(roadid>0) item.setRoadid(roadid);  //回到主路不记录
				item.setMoving(stop);
				item.setOnline(true);
				 
				break;
			}
		}
	}

	/**
	 * 更新交管区域
	 * @param ip
	 * @param areaID
	 */
	public static synchronized void updateAreaId(String ip, int areaID) {
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				item.setAreaId(areaID);
				 
				break;
			}
		}
	}
	 
	
	
	/**
	 * 更新小车下道工序
	 * 
	 * @param parkNo
	 * @return
	 */
	public static synchronized void updateAgvNextProcess(int agvid,String nextprocess ) {
		 
		for (AgvItem item : list) {
			if (item.getAgvId() == agvid ) { 
				 item.setNextProcess(nextprocess);
				 AgvTools.saveFile(item.toBean());
				 return;
			}
		}
	 
	}
	
	
	/**
	 * 小车异常，只更新状态，资源释放需要在服务区里处理
	 * @param ip
	 */
	public static synchronized void abnormal(String ip) {
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) { 
				item.setOnline(false);
				item.setMoving(false);
				 
				break;
			}
		}
	}

	/**
	 * 到达停泊点
	 * 
	 * @param ip
	 * @param parkNO 0=离开
	 */
	public static synchronized void arrivedPark(String ip, int parkNO ) {
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				item.setParkNo(parkNO); 
				item.setMoving(false);
				 
				break;
			}
		}
	}

	/**
	 * 从交管中移除小车后的操作
	 * @param p
	 */
	private static void clearTraffic(AgvItem p,boolean force) {
		// 所有交管区域都移除该小车
		if(force) {
			//强制移除停车
			TranfficAreaCache.removeStopAgv(p.getAreaId(), p.getAgvId());
		}else {
			TranfficAreaCache.removeStopAgv(p.getAgvId());
		}
		boolean locker=TranfficAreaCache.isLocker(p.getAreaId(), p.getAgvId()); 
		//如果小车是锁路段
		if (locker) {
			// 启动下一辆小车
			int nextAgvId = TranfficAreaCache.nextAgv(p.getAreaId());
			if (nextAgvId>0) {
				AgvItem nextAgv = null;
				for (AgvItem x1 : list) {
					if (x1.getAgvId()==nextAgvId) {
						nextAgv = x1;
						break;
					}
				}
				if (nextAgv != null) {
					//交管区重新上锁
					TranfficAreaCache.relockArea(p.getAreaId(), nextAgvId);
					// 启动nextAgv 
					MsgData restart = new MsgData(nextAgv.getIP(), nextAgv.getPort(),
							AgvCmd.stopOrstart(nextAgv.getAgvId(), false),"交管启动");
					restart.setAgvId(nextAgv.getAgvId());
					MsgQueue.Push(restart);
				} else {
					//没有下一辆小车，交管区直接释放
					TranfficAreaCache.unlockArea(p.getAgvId());
				}
			}
		}
	}

	/**
	 * 强制释放小车的各种资源（进入了服务区）
	 * @param ip
	 */
	public static synchronized void clearSource(String ip) {
		AgvItem p = null;
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				p = item;
				break;
			}
		}
		if (p != null) {
			// 交管信息
			clearTraffic(p,true);
			p.setAreaId(0);
			 
			//预判断点
			EstimateCache.removeAgv(p.getParkNo(), p.getAgvId());
			
			
			// 停泊位释放
			ParkCache.removeAgv(ip);

			//待命位释放
			ReadyDotCache.removeAgvFromDot(p.getAgvId());
			 
			// 设备分组释放
			StationGroupCache.unlockForAgv(ip);

			// 装载情况释放
			MountCache.removeAgv(ip);
			 
		}
	}

	/**
	 * 离开交管区
	 * @param ip
	 */
	public static synchronized void leaveArea(String ip) {
		AgvItem p = null;
		for (AgvItem item : list) {
			if (item.getIP().equals(ip)) {
				p = item;
				break;
			}
		}
		if (p != null) {
			// 交管信息
			clearTraffic(p,false);
			p.setAreaId(0);
		 
		}
	}
	
	
	
	/**
	 * 保存小车状态
	 * @param agvid
	 */
	public static void saveState(int agvid) {
		for (AgvItem x1 : list) {
			if (x1.getAgvId()==agvid) {
				AgvTools.saveFile(x1.toBean());
				break;
			}
		}
	}
}
