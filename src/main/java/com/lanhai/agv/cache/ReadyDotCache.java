package com.lanhai.agv.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.lanhai.agv.localstore.ReadyDotTools;

/**
 * 装卸分配点管理（一个分配点一个工序）
 * 
 * @author virus408
 *
 */
public class ReadyDotCache {
	// 分配点位：可用待命点位
	static Map<Integer, List<ReadyDotItem>> map;
 

	// 分配点位：工序分组
	static Map<Integer, String> stationGroupMap;

 	 
	
	static {
		map = new HashMap<Integer, List<ReadyDotItem>>();  
		stationGroupMap = new HashMap<Integer, String>(); 
	}

	
	/**
	 * 新增分配点位：工序分组
	 * @param parkNo
	 * @param processName
	 */
	public static void addProcessGroup(int parkNo,String processName) {
		stationGroupMap.put(parkNo, processName);
	}
	
	/**
	 * 新增分配点位：可用待命点位
	 * @param parkNo
	 * @param ar
	 */
	public static void addReadyDotList(int parkNo,List<ReadyDotItem> ar) {
		map.put(parkNo, ar);
	}
	
 
	 
	
	
	/**
	 * 待命点没有小车
	 * 
	 * @param parkno
	 * @return
	 */
	public static boolean isEmpty(Integer parkno,String processName) {
		List<ReadyDotItem> re = map.get(parkno);
		if (re == null || re.size() == 0)
			return false;
		for (ReadyDotItem item : re) {
			if (item.getAgvId() > 0&& item.getProcessName().equals(processName)) {
				return false;
			}
		}
		return true;
	}
	
	 

	/**
	 * 找出待命点停得最久的小车id
	 * 
	 * @param parkno
	 * @return
	 */
	public static ReadyDotItem getFirstAgv(int parkno,String processName) {
		List<ReadyDotItem> re = map.get(parkno);
		if (re == null || re.size() == 0)
			return null;

		ReadyDotItem p=null;
		long min = System.currentTimeMillis();
		for (ReadyDotItem item : re) {
			if (item.getAgvId() > 0 && item.getProcessName().equals(processName)) {
				if (item.getLastTime() < min) {
					min = item.getLastTime();
					p=item;
				}
			}
		}
		return p;
	}
	
	/**
	 * 判断是否有空的待命点
	 * @param parkno
	 * @return
	 */
	public static boolean hasDot(int parkno,String processName) {
		List<ReadyDotItem> re = map.get(parkno);
		if (re == null || re.size() == 0)
			return false;
 
		for (ReadyDotItem item : re) {
			if (item.getAgvId() ==0&& item.getProcessName().equals(processName)) {
				 return true;
			}
		}
		return false;
	}
	

	/**
	 * 获取空的待命点
	 * @param parkno
	 * @return
	 */
	public static ReadyDotItem getEmptyDotItem(int parkno) {
		List<ReadyDotItem> re = map.get(parkno);
		if (re == null || re.size() == 0)
			return null;
		for (ReadyDotItem item : re) {
			if (item.getAgvId() == 0) {
				return item.copy();
			}
		}
		return null;
	}

	/**
	 * 释放小车的待命点
	 * 
	 * @param agvIp
	 */
	public static void removeAgv(int readyDotParkno, int agvId) { 
		List<ReadyDotItem> re = map.get(readyDotParkno);
		for (ReadyDotItem item : re) {
			if (item.getAgvId() == agvId) {
				item.setLastTime(0);
				item.setAgvId(0);
				item.setProcessName("");
				
				ReadyDotTools.saveFile(item);
				return;
			}
		}
	}

	/**
	 * 分配待命位
	 * 
	 * @param agvIp
	 * @param parkno
	 * @return
	 */
	public static boolean dispathDot(int agvId, int readyDotParkNo) {
		List<ReadyDotItem> re = map.get(readyDotParkNo);
		for (ReadyDotItem item : re) {
			if (item.getAgvId() == 0) {
				item.setAgvId(agvId);
				item.setLastTime(System.currentTimeMillis());
				ReadyDotTools.saveFile(item);
				return true; 
			}
		}
		return false;
	}
	
	/**
	 * 判断小车是否来自待命点
	 * @param parkno
	 * @param agvId
	 * @return
	 */
	public static boolean isFromDot(int parkno,int agvId) {
		List<ReadyDotItem> re = map.get(parkno);
		for (ReadyDotItem item : re) {
			if (item.getAgvId() == agvId) {
				 return true;
			}
		}
		return false;
	}

	 
	/**
	 * 释放待命位上的小车
	 * @param agvId
	 */
	public static void removeAgvFromDot(  int agvId) { 
		for(Entry<Integer,List<ReadyDotItem>> entry:map.entrySet()) {
			for (ReadyDotItem item : entry.getValue()) {
				if (item.getAgvId() == agvId) {
					item.setLastTime(0);
					item.setAgvId(0);
					item.setProcessName("");
					ReadyDotTools.saveFile(item);
					return;
				}
			}
		}
		 
	}
}
