package com.lanhai.agv.cache;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lanhai.agv.localstore.EstimateLocalBean;
import com.lanhai.agv.localstore.EstimateTools;

/**
 * 预判断点，没有涉及区段
 * @author virus408
 *
 */
public class EstimateCache {
  
	//点位：野车列表
	static Map<Integer,List<Integer>> agvList;
	  
	static {  
		agvList=new HashMap<Integer,List<Integer>>();
	}

 
	
	public static void add(int parkNo,List<Integer> ls) {
		agvList.put(parkNo, ls);
	}
	
 
	/**
	 * 获取停下来的第一辆车(同时释放）
	 * @param parkNo
	 * @return
	 */
	public static synchronized int nextAgv(int parkNo) {
		List<Integer> ls=agvList.get(parkNo);
		if(ls==null||ls.size()==0) {
			return 0;
		}else {
			int agvid=ls.get(0);
			ls.remove(0);
			agvList.put(parkNo, ls);
			
			EstimateLocalBean bean=new EstimateLocalBean();
			bean.setParkNo(parkNo);
			bean.setStopAgv(ls); 
			EstimateTools.saveFile(bean);
			
			
			return agvid;
		} 
	}
	
	/**
	 * 记录停下来的车号
	 * @param parkNo
	 * @return
	 */
	public static synchronized void stopAgv(int parkNo,int agvId) {
		List<Integer> ls=agvList.get(parkNo);
		if(ls!=null) { 
			 ls.add(agvId); 
			agvList.put(parkNo, ls); 
			
			EstimateLocalBean bean=new EstimateLocalBean();
			bean.setParkNo(parkNo);
			bean.setStopAgv(ls); 
			EstimateTools.saveFile(bean);
		} 
	}


	/**
	 * 从野车队列里移除小车
	 * @param parkNo
	 * @param agvId
	 */
	public static synchronized void removeAgv(int parkNo, int agvId) {
		List<Integer> ls = agvList.get(parkNo);
		if (ls != null) {
			ls.remove(agvId);
			agvList.put(parkNo, ls);
			
			EstimateLocalBean bean=new EstimateLocalBean();
			bean.setParkNo(parkNo);
			bean.setStopAgv(ls); 
			EstimateTools.saveFile(bean);
		}
	}
}
