package com.lanhai.agv.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * 路线和工序对应项,用于检索
 * @author virus408
 *
 */
public class RoadCache { 
	//路线：区段
	static Map<Integer,Integer> lvmap;
	
	
	static { 
		lvmap=new HashMap<Integer,Integer>(); 
	}
	
	
	public static void add(int roadid, int lv) {
		 
		lvmap.put(roadid, lv);
	}
	
	 
	
	
	public static int getLv(int roadid) {
		Integer m=lvmap.get(roadid);
		if(m==null) return 0;
		return m;
	}
	
}
