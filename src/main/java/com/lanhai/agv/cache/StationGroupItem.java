package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * 工序分组下的，设备分组项
 * 
 * @author virus408
 *
 */
public class StationGroupItem {

	private List<StationGroupSub> list;

	public  StationGroupItem() {
		list = new ArrayList<StationGroupSub>();
	}
 
	
	public void setList(List<StationGroupSub> list) {
		this.list = list;
	}


	/**
	 * 更新可用和不可用
	 * @param name
	 * @param Disable
	 */
	public void updateDisable(String stationGroup,boolean Disable) {
		for(StationGroupSub item : list) {
			if(item.getStationGroup().equals(stationGroup)) {
				item.setDisable(Disable);
				break;
			}
		}
	}

	/**
	 * 返回可用的（未锁未被禁用）分组名称。
	 * @return
	 */
	public List<String> getCanUseNames(){
		List<String> ls=new ArrayList<String>();
		for(StationGroupSub item : list) {
			if(item.isLock()==false&&item.isDisable()==false) {
				ls.add(item.getStationGroup());
			}
		} 
		return ls;
	}
 
	public  synchronized void lockGroup(String stationGroup,String agvIp ) {
		for(StationGroupSub item : list) {
			if(item.getStationGroup().equals(stationGroup)) {
				 item.setLock(true);
				  
					 item.setAgvIp(agvIp);
				 
				 break;
			}
		} 
	}
	
	public  synchronized void unlockForAgv( String agvIp ) {
		for(StationGroupSub item : list) {
			if(item.getAgvIp().equals(agvIp)) {
				 item.setLock(false );
				 item.setAgvIp("");
				 break;
			}
		} 
	}
	
	
	 
}
