package com.lanhai.agv.cache;

import com.lanhai.agv.dispatch.AgvCmd;
import com.lanhai.agv.dispatch.AgvMountState;
import com.lanhai.agv.push.MsgData;
import com.lanhai.agv.push.MsgQueue;
import com.lanhai.agv.thirdpart.ApplyTransferEnum;
import com.lanhai.agv.thirdpart.StationInterface;
import com.lanhai.agv.thirdpart.StationResolveFactory;

/**
 * 小车装载情况
 * 
 * @author virus408
 *
 */
public class AgvMount {

	private String agvIP;
	private int agvId;
	private AgvMountState state;

	public String getAgvIP() {
		return agvIP;
	}

	public void setAgvIP(String agvIP) {
		this.agvIP = agvIP;
	}

	public AgvMountState getState() {
		return state;
	}

	public void setState(AgvMountState state) {
		this.state = state;
	}

	/**
	 * 处理传输，数量判断之前已经处理，本方法不涉及数据判断
	 * 
	 * @param park
	 */
	public String judgement(ParkItem park) {
		// 拿到对接设备的解析方式
		StationInterface station = StationResolveFactory.getStationResolve(park.getCmdSign());
		if (station == null)
			return "缺少解析方式";

		AgvItem agv = AgvCache.getAgvByIP(agvIP);

		if (park.getTransType() == ParkTransPosEnum.OnlyIn) {
			// 纯上料
			if (park.getTransInNum() == 0 || park.getTransInNum() % 10 != 0) {
				// 数量不满足
				return "等待对接设备准备";
			}

			if (park.isStationUpTrans() && state.isUpFull() && state.isFinishSend() == false) {
				// 需要对接设备上层收货
				MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.In),
						"开始传输");
				MsgQueue.Push(s1);

				MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUp(agv.getAgvId(), false), "上层发货");
				s2.setAgvId(agv.getAgvId());
				MsgQueue.Push(s2);
				return "开始传输";
			}
			if (park.isStationDownTrans() && state.isDownFull() && state.isFinishSend() == false) {
				// 需要对接设备下层收货
				MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.In),
						"开始传输");
				MsgQueue.Push(s1);

				MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transDown(agv.getAgvId(), false), "下层发货");
				s2.setAgvId(agv.getAgvId());
				MsgQueue.Push(s2);

				return "开始传输";
			}
		} else if (park.getTransType() == ParkTransPosEnum.OnlyOut) {
			// 纯下料
			if (park.getTransOutNum() == 0 || park.getTransOutNum() % 10 != 0) {
				// 数量不满足
				return "等待对接设备准备";
			}

			if (park.isStationUpTrans() && state.isUpFull() == false && state.isFinishReceive() == false) {

				// 需要对接设备上层发货
				MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.Out),
						"开始传输");
				MsgQueue.Push(s1);

				MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUp(agv.getAgvId(), true), "上层收货");
				s2.setAgvId(agv.getAgvId());
				MsgQueue.Push(s2);

				return "开始传输";
			}
			if (park.isStationDownTrans() && state.isDownFull() == false && state.isFinishReceive() == false) {

				// 需要对接设备下层发货
				MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.Out),
						"开始传输");
				MsgQueue.Push(s1);

				MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transDown(agv.getAgvId(), true), "下层收货");
				s2.setAgvId(agv.getAgvId());
				MsgQueue.Push(s2);

				return "开始传输";
			}
		} else if (park.getTransType() == ParkTransPosEnum.Both) {
			// 下上料

			// 最好的情况，双方匹配，数量对得上
			if (park.isNeedOut() && park.isNeedIn() && state.isFinishReceive() == false && state.isFinishSend() == false
					&& park.getTransOutNum() > 0 && park.getTransOutNum() % 10 == 0 && park.getTransInNum() > 0
					&& park.getTransInNum() % 10 == 0) {

				// 同时传输
				// 先通知对接设备收货
				MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.Both),
						"开始传输");
				MsgQueue.Push(s1);
				if (state.isUpFull()) {
					// 上出下入
					MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUpOutAndDownIn(agv.getAgvId()),
							"上出下入");
					MsgQueue.Push(s2);
				} else {
					// 上入下出
					MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUpInAndDownOut(agv.getAgvId()),
							"上入下出");
					s2.setAgvId(agv.getAgvId());
					MsgQueue.Push(s2);
				}
				return "开始传输";
			}

			//// 以下是非同传时的操作，分成两步，防止出错

			// 1.小车先收货
			if (state.isFinishReceive() == false && park.isNeedOut()) {

				if (park.getTransOutNum() > 0 && park.getTransOutNum() % 10 == 0) {
					// 通知对接机传输发货
					MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.Out),
							"开始传输");
					MsgQueue.Push(s1);
					if (state.isUpFull() == false) {
						// 小车上层收货
						MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUp(agv.getAgvId(), true),
								"上层收货");
						s2.setAgvId(agv.getAgvId());
						MsgQueue.Push(s2);
						return "开始传输";
					}
					if (state.isDownFull() == false) {
						// 小车下层收货
						MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transDown(agv.getAgvId(), true),
								"下层收货");
						s2.setAgvId(agv.getAgvId());
						MsgQueue.Push(s2);
						return "开始传输";
					}
				}
				return "等待对接设备准备";
			}

			// 2.小车再发货
			if (state.isFinishSend() == false && park.isNeedIn()) {
				if (park.getTransInNum() > 0 && park.getTransInNum() % 10 == 0) {
					// 通知对接机传输收货
					MsgData s1 = new MsgData(park.getStationIP(), park.getStationPort(), station.applyTransfer(ApplyTransferEnum.In),
							"开始传输");
					MsgQueue.Push(s1);

					if (state.isUpFull()) {
						// 小车上层发货
						MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transUp(agv.getAgvId(), false),
								"上层发货");
						s2.setAgvId(agv.getAgvId());
						MsgQueue.Push(s2);
						return "开始传输";
					}
					if (state.isDownFull()) {
						// 小车下层发货
						MsgData s2 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.transDown(agv.getAgvId(), false),
								"下层发货");
						s2.setAgvId(agv.getAgvId());
						MsgQueue.Push(s2);
						return "开始传输";
					}
				}
				return "等待对接设备准备";
			}
		} else {
			// 错误类型
		}

		//// 如果以上没有执行到，即没有return,则个时候就需要申请离开了
		// 申请离开
		if (state.isFinishReceive() && state.isFinishSend()) {
			MsgData s3 = new MsgData(agv.getIP(), agv.getPort(), AgvCmd.runRoad(agv.getAgvId(), 0), "申请离开");
			MsgQueue.Push(s3);
			return "传输完成，申请离开";
		} else {
			return "等待传输结束";
		}

	}

	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}
 
	 
}
