package com.lanhai.agv.cache;

import com.lanhai.agv.localstore.AgvLocalBean; 

/**
 * 缓存的小车项
 * @author virus408
 *
 */
public class AgvItem  {
	 
	private int agvId;
	private int port;
	private int currentX;
	private int currentY;
	private String IP;
	private boolean online;
	
	private int areaId; 
	private boolean moving; 
	
	private int parkNo; 
	private int roadid;
	 
	//目标工序分组
	private String nextProcess;
	
	//充电次数
	private int powerTime;
	
	public AgvItem() {
		parkNo=0;
		online=false;
		areaId=0;
		moving=false;
		currentX=0;
		currentY=0;
	}
	
	
	public void loadDB( AgvLocalBean obj) {
		areaId=obj.getAreaId();
		parkNo=obj.getParkNo();
		currentX=obj.getCurrentX();
		currentY=obj.getCurrentY();
		online=obj.isOnline();
		moving=obj.isMoving(); 
		roadid=obj.getRoadid(); 
		nextProcess=obj.getNextProcess();
		powerTime=obj.getPowerTime();
	}
	
	public AgvLocalBean toBean() {
		AgvLocalBean obj=new AgvLocalBean();
		obj.setAreaId(areaId);
		obj.setParkNo(parkNo);
		obj.setCurrentX(currentX);
		obj.setCurrentY(currentY);
		obj.setOnline(online);
		obj.setMoving(moving); 
		obj.setRoadid(roadid); 
		obj.setAgvId(agvId);
		obj.setNextProcess(nextProcess);
		obj.setPowerTime(powerTime);
		return obj;
	}
	
	
	public AgvItem copy() {
		AgvItem newobj=new AgvItem();
		newobj.setAgvId(agvId); ;
		newobj.setCurrentX(currentX); 
		newobj.setCurrentY(currentY); 
		newobj.setIP(IP);  
		newobj.setPort(port);
		newobj.setParkNo(parkNo); 
		newobj.setAreaId(areaId); 
		newobj.setMoving(moving); 
		newobj.setRoadid(roadid);
		newobj.setNextProcess(nextProcess);
		return newobj;
	}
	
	
	public int getAreaId() {
		return areaId;
	}


	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

 

	public int getRoadid() {
		return roadid;
	}


	public void setRoadid(int roadid) {
		this.roadid = roadid;
	}


	public int getAgvId() {
		return agvId;
	}
	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}
	public int getCurrentX() {
		return currentX;
	}
	public void setCurrentX(int currentX) {
		this.currentX = currentX;
	}
	public int getCurrentY() {
		return currentY;
	}
	public void setCurrentY(int currentY) {
		this.currentY = currentY;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	 
	public boolean isOnline() {
		return online;
	}


	public void setOnline(boolean online) {
		this.online = online;
	}


	public int getParkNo() {
		return parkNo;
	}
	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}


	public boolean isMoving() {
		return moving;
	}


	public void setMoving(boolean moving) {
		this.moving = moving;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public String getNextProcess() {
		return nextProcess;
	}


	public void setNextProcess(String nextProcess) {
		this.nextProcess = nextProcess;
	}


	public int getPowerTime() {
		return powerTime;
	}


	public void setPowerTime(int powerTime) {
		this.powerTime = powerTime;
	}


	 
	
	 
}
