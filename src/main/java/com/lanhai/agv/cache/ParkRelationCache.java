package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * 预判断点、装卸分配点、资源释放点的关系表
 * @author virus408
 *
 */
public class ParkRelationCache {
	//预判断点、装卸分配点 的关系
	static List<ParkRelationItem> estimateList;
	
	//装卸分配点、资源释放点 的关系
	static List<ParkRelationItem> releaseList;
	
	
	 static {
		 estimateList=new ArrayList<ParkRelationItem>();
		 releaseList=new ArrayList<ParkRelationItem>();
	 }
	 
	 public static void addEstimate(List<ParkRelationItem>  list) {
		 if(list!=null&&list.size()>0) {
			 estimateList.addAll(list);
		 }
	 }
	 
	 public static void addReleaseList(List<ParkRelationItem>  list) {
		 if(list!=null&&list.size()>0) {
			 releaseList.addAll(list);
		 }
	 }
	 
	 
	 /**
	  * 通过装卸分配点拿预分配点
	 * @param readydotParkNo
	 * @return
	 */
	public static int getEstimateParkNo(int readydotParkNo) {
		  for(ParkRelationItem item :estimateList) {
			  if(item.getEnddot()==readydotParkNo) {
				  return item.getStartdot();
			  }
		  } 
		 return 0;
	 }
	
	 /**
	  * 通过释放点拿装卸分配点
	 * @param readydotParkNo
	 * @return
	 */
	public static int getReadyDotParkNo(int releaseParkNo) {
		  for(ParkRelationItem item :releaseList) {
			  if(item.getEnddot()==releaseParkNo) {
				  return item.getStartdot();
			  }
		  } 
		 return 0;
	 }


	public static int getReadyDotParkNoByEstimate(int estimateNo) {
		 for(ParkRelationItem item :estimateList) {
			  if(item.getStartdot()==estimateNo) {
				  return item.getStartdot();
			  }
		  } 
		 return 0;
	}
}
