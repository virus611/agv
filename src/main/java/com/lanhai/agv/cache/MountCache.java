package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.dispatch.AgvMountState;
import com.lanhai.agv.localstore.AgvMountLocalBean;
import com.lanhai.agv.localstore.AgvMountTools;

/**
 * 装载情况，不需要初始化
 * @author virus408
 *
 */
public class MountCache  {
	static List<AgvMount> list;
	
	static {
		list=new ArrayList<AgvMount>();
	}
	
	public static void add(List<AgvMountLocalBean> ls) {
		if(ls==null||ls.size()==0) return;
		for(AgvMountLocalBean bean:ls) {
			AgvMount obj=new AgvMount(); 
			obj.setAgvId(bean.getAgvId());
			obj.setAgvIP(bean.getAgvIp());
			
			AgvMountState sub=new AgvMountState();
			sub.setDownFull(bean.isDownFull());
			sub.setUpFull(bean.isUpFull());
			sub.setFinishReceive(bean.isFinishReceive());
			sub.setFinishSend(bean.isFinishSend());
			obj.setState(sub);
			
			list.add(obj);
		}
		
	}
	
	
	public static synchronized void mountAgv(int agvId, String agvIP,AgvMountState state) {
		AgvMount ob=null;
		for(AgvMount item :list) {
			if(item.getAgvId()==agvId) {
				ob=item;
				break;
			}
		}
		if(ob==null) {
			ob=new AgvMount();
			ob.setAgvId(agvId);
			ob.setAgvIP(agvIP);
			ob.setState(state);
			list.add(ob);
		}else{
			ob.setState(state);
		}
		 
		AgvMountLocalBean bean=new AgvMountLocalBean();
		bean.setAgvId(agvId);
		bean.setAgvIp(agvIP);
		bean.setDownFull(state.isDownFull());
		bean.setUpFull(state.isUpFull());
		bean.setFinishReceive(state.isFinishReceive());
		bean.setFinishSend(state.isFinishSend());
		AgvMountTools.saveFile(bean);
	}
	
	/**
	 * 移除装载情况，只有释放的时候才调用
	 * @param agvIP
	 */
	public static synchronized void removeAgv(String agvIP) {
		AgvMount item=null;
		for(int i=0;i<list.size();i++) {
			  item=list.get(i);
			if(item.getAgvIP().equals(agvIP)) {
				list.remove(i);
				
				AgvMountLocalBean bean=new AgvMountLocalBean();
				bean.setAgvId(item.getAgvId());
				bean.setAgvIp(item.getAgvIP());
				bean.setDownFull(false);
				bean.setUpFull(false);
				bean.setFinishReceive(false);
				bean.setFinishSend(false);
				AgvMountTools.saveFile(bean);
				return;
			}
		}
	}
	
	public static String judgement(String agvIP,ParkItem park) {
		AgvMount item=null;
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getAgvIP().equals(agvIP)) {
				item=list.get(i);
				break;
			}
		}
		if(item!=null) {
			return item.judgement(park);
		}else {
			return "错误的数据";
		}
	}
}
