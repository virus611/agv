package com.lanhai.agv.cache;
 

/**
 * 停泊位类型
 * @author virus408
 *
 */
public enum ParkTypeEnum {
	Trans(1),
	Power(2),
	Wait(3), 
	TransReady(4),
	TransOut(5), 
	Estimate(6);
	
	ParkTypeEnum(int i) { 
		type=i;
	}
	
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public static ParkTypeEnum getParkTypeEnum(int b) {
		ParkTypeEnum as = null;
    	switch (b) {
    	case 1:
    		as=ParkTypeEnum.Trans;
    		break;
    	case 2:
    		as=ParkTypeEnum.Power;
    		break;
    	case 3:
    		as=ParkTypeEnum.Wait;
    		break;
    	case 4:
    		as=ParkTypeEnum.TransReady;
    		break;
    	case 5:
    		as=ParkTypeEnum.TransOut;
    		break;
    	case 6:
    		as=ParkTypeEnum.Estimate;
    	default:
    		as=ParkTypeEnum.Wait;
    	}
    	 
    	return as;
	}
}
