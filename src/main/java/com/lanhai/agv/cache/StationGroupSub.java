package com.lanhai.agv.cache;

/**
 * 设备分组项
 * 
 * @author virus408
 *
 */ 
public class StationGroupSub {

	
		private String stationGroup;
		private boolean disable;
		private boolean lock;
		private String agvIp;

	 
		public String getStationGroup() {
			return stationGroup;
		}

		public void setStationGroup(String stationGroup) {
			this.stationGroup = stationGroup;
		}

		public boolean isDisable() {
			return disable;
		}

		public void setDisable(boolean disable) {
			this.disable = disable;
		}

		public boolean isLock() {
			return lock;
		}

		public void setLock(boolean lock) {
			this.lock = lock;
		}

		public String getAgvIp() {
			return agvIp;
		}

		public void setAgvIp(String agvIp) {
			this.agvIp = agvIp;
		}

}
