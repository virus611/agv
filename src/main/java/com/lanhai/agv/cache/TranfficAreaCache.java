package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lanhai.agv.localstore.TranfficAreaTools;

/**
 * 交管区域管理
 * @author virus408
 *
 */
public class TranfficAreaCache {
	static List<TranfficArea> list;
	
	//区域：锁定的小车
	static Map<Integer,Integer> lockMap;
	
	
	static {
		list=new ArrayList<TranfficArea>();
		lockMap=new HashMap<Integer,Integer>();
	}
	
	
	public static synchronized void addList(List<TranfficArea> ls) {
		if(ls!=null&&ls.size()>0) {
			list.addAll(ls);
			
			//区域锁小车情况
			for(TranfficArea ta:ls) {
				if(ta.getAgvId()>0) {
					lockMap.put(ta.getAreaId(), ta.getAgvId());
				}
			} 
		}
		
	}
	
	 
	
	public static synchronized int intoArea(int x,int y) {
		 for(TranfficArea item :list) {
			int id=item.getAreaId(x, y);
			if(id>0) {
				return id;
			}
		 }
		 return 0;
	}
	
	
	/**
	 * 锁定区域
	 * @param areaId
	 * @param agvIP
	 * @return
	 */
	public static synchronized boolean lockArea(int areaId, int agvId) {
		TranfficArea area=null;
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) {
				 area=item;
				 break;
			 }
		 }
		 //没有区域
		 if(area==null) return true;
		 
		 //区域锁定的小车是该车
		 if( area.getAgvId()==agvId) {
			 return true ;
		 }else {
			 //没被锁
			 if(area.getAgvId()==0) {
				 area.setAgvId(agvId);
				 lockMap.put(area.getAreaId(), agvId);
				 
				 TranfficAreaTools.saveFile(area.toBean());
				 return true;
			 }else {
				 return false;
			 } 
		 } 
	}
	
	
	public static synchronized boolean relockArea(int areaId, int agvId) {
		TranfficArea area=null;
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) {
				 area=item;
				 break;
			 }
		 }
		 if(area==null) return false; 
		 //更换锁路线的小车 
		 area.setAgvId(agvId); 
		 lockMap.put(area.getAreaId(), agvId);
		 TranfficAreaTools.saveFile(area.toBean());
		 return true;
	}
	
	
	/**
	 * 解锁区域
	 * @param areaId
	 */
	public static synchronized void unlockArea(int areaId) {
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) { 
				 item.setAgvId(0);
				 lockMap.put(areaId,0);
				 TranfficAreaTools.saveFile(item.toBean());
				 break;
			 }
		 }
	} 
	
	
	/**
	 * 移除所有区域的停下来某小车
	 * @param agvId
	 */
	public static synchronized void removeStopAgv(int agvId) {
		 for(TranfficArea item :list) {
			 boolean flag= item.removeAgv(agvId);
			 if(flag) {
				 lockMap.put(item.getAreaId(),0);
				 TranfficAreaTools.saveFile(item.toBean());
				 return;
			 }
		 }
	} 
	
	/**
	 * 移除某区域停下来的的某小车
	 * @param areaid
	 * @param agvId
	 */
	public static synchronized void removeStopAgv(int areaid, int agvId) {
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaid) {
				 item.removeAgv(agvId);
				 lockMap.put(item.getAreaId(),0);
				 TranfficAreaTools.saveFile(item.toBean());
				 return;
			 } 
		 }
	} 
	
	/**
	 * 判断小车是否是某路段的上锁者
	 * @param areaid
	 * @param agvId
	 * @return
	 */
	public static synchronized boolean isLocker(int areaid, int agvId ) {
		if(lockMap.containsKey(areaid)) {
			int k=lockMap.get(areaid);
			return k==agvId;
		}else {
			return false;
		} 
		
	}
 
	
	public static synchronized int nextAgv(int areaId ) {
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) {
				 return item.nextAgv();
			 }
		 }
		 return 0;
	}
	
	public static synchronized void addStopAgv(int areaId ,int agvId ) {
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) {
				 item.addStopAgv(agvId);
				 TranfficAreaTools.saveFile(item.toBean());
				 break;
			 }
		 } 
	}
	
	/**
	 * 是否服务区
	 * @param areaId
	 * @return
	 */
	public static synchronized boolean isService(int areaId) {
		 for(TranfficArea item :list) {
			 if(item.getAreaId()==areaId) {
				 return item.getAreaType()>0;
			 }
		 } 
		 return false;
	}

 
}
