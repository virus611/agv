package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.lanhai.agv.localstore.ParkTools;
 

/**
 * 所有停泊位的管理
 * 
 * @author virus408
 *
 */
public class ParkCache {
	static List<ParkItem> list;
	 
	static {
		list = new ArrayList<ParkItem>();
	}

	public static synchronized void addList(List<ParkItem> ls) {
		if(ls!=null&&ls.size()>0)
		list.addAll(ls);
	}

	/**
	 * 通过点位计算停泊位
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static synchronized ParkItem getParkByPos(int x, int y,int lv) {
		ParkItem p = null;
		for (ParkItem item : list) {
			if (item.getLv()==lv&&item.inArea(x, y)) {
				p = item.copy();
				break;
			}
		}
		return p;
	}

	/**
	 * 通过Ip和端口获取停泊位
	 * 
	 * @param ip
	 * @param port
	 * @return
	 */
	public static synchronized ParkItem getParkByStation(String ip, int port) {
		ParkItem p = null;
		for (ParkItem item : list) {
			if (item.getStationIP().equals(ip) && item.getStationPort() == port) {
				p = item.copy();
				break;
			}
		}
		return p;
	}

	public static synchronized ParkItem getParkByNo( int parkno ) {
		ParkItem p = null;
		for (ParkItem item : list) {
			if (item.getParkNo()==parkno ) {
				p = item.copy();
				break;
			}
		}
		return p;
	}
	
	
	public static synchronized ParkItem getParkByAgv(String ip) {
		ParkItem p = null;
		for (ParkItem item : list) {
			if (item.getAgvIP().equals(ip)) {
				p = item.copy();
				break;
			}
		}
		return p;
	}

	/**
	 * 设置停泊位的小车
	 * 
	 * @param parkNo
	 * @param ip
	 */
	public static synchronized void setAgv(int parkNo,   String ip) {

		for (ParkItem item : list) {
			if (item.getParkNo() == parkNo ) {
				item.setAgvIP(ip);
				break;
			}
		}
	}

	/**
	 * 释放小车占用的资源
	 * 
	 * @param ip
	 */
	public static synchronized void removeAgv(String ip) {

		for (ParkItem item : list) {
			if (item.getAgvIP().equals(ip)) {
				item.setAgvIP("");
				item.setNeedIn(false);
				item.setNeedOut(false);
			}
		}
	}

	/**
	 * 释放停泊位小车
	 * 
	 * @param parkNo
	 */
	public static synchronized void removeAgv(int parkNo  ) { 
		for (ParkItem item : list) {
			if (item.getParkNo() == parkNo ) {
				item.setAgvIP("");
				item.setNeedIn(false);
				item.setNeedOut(false);
				item.setLastTime(System.currentTimeMillis());
				break;
			}
		}
	}
	
	 

	/**
	 * 设置对接设备装料情况，在传输过程中，则不操作
	 * 
	 * @param ip
	 * @param port
	 * @param state
	 */
	public static synchronized boolean updateStationMount(String ip, int port, boolean needIn,boolean needOut,int inNum,int outNum ) {
		boolean same=true;
		for (ParkItem item : list) {
			if (item.getStationIP().equals(ip) && item.getStationPort() == port 
					&& item.isTransing()==false ) {
				//停泊点上没小车是大前提,更新数量不影响 
				item.setTransInNum(inNum);
				item.setTransOutNum(outNum);
				item.setNeedIn(needIn);
				item.setNeedOut(needOut); 
				 
				ParkTools.saveFile(item.toBean());
				break;
			}
		}
		return same;
	}
	
	/**
	 * 返回某设备分组可用的停泊位（有来源路线的）
	 * @param groupNameList
	 * @param type 0=按权重分配  1=按数量分配
	 * @return
	 */
	public static synchronized  ParkItem  getCanTransPark(List<String> groupNameList,int selecttype){
		List<ParkItem> arr=new ArrayList<ParkItem>();
		if(groupNameList==null||groupNameList.size()==0) return null;
		for (ParkItem item : list) {
			if (item.getSourceRoad()>0 && (item.isNeedIn()||item.isNeedOut())
					&& groupNameList.contains(item.getStationGroup())){
				//有来源路线，并且在列表里的，并且能传输的，就符合要求
				arr.add(item .copy());
			}
		} 
		if(arr.size()>0) {
			 //排序
			if(selecttype==0) {
				// 权重升序,权重相同，最后传输时间升序
				Collections.sort(arr,new Comparator<ParkItem>() {

					@Override
					public int compare(ParkItem o1, ParkItem o2) {
						// o1-o2是升序
						if(o2.getWeight()==o1.getWeight()) {
							return (int) (o1.getLastTime()-o2.getLastTime());
						}else {
							return o1.getWeight()-o2.getWeight();
						} 
					}
					
				});
			}else if(selecttype==1) {
				//花篮数量降序，数量相同，最后传输时间升序
				Collections.sort(arr,new Comparator<ParkItem>() {

					@Override
					public int compare(ParkItem o1, ParkItem o2) {
						//o1-o2是升序
						int num2=o2.getTransInNum()+o2.getTransOutNum();
						int num1=o2.getTransInNum()+o1.getTransOutNum();
						if( num2 == num1 ) {
							return (int) (o1.getLastTime()-o2.getLastTime());
						}else {
							return num2-num1;
						} 
					} 
				});
			} 
			return arr.get(0);
		}else {
			return null;
		}
		
	}

	/**
	 * 某点位的下一条路线
	 * @param parkno
	 * @return
	 */
	public static int getTargetRoad(int parkno) {
		for (ParkItem item : list) {
			 if(item.getParkNo()==parkno) {
				 return item.getTargetRoad();
			 }
		} 
		return -1;
	}
}
