package com.lanhai.agv.cache;

import java.util.HashMap;
import java.util.Map;

/**
 *  上报指令缓存
 * @author virus408
 *
 */
public class EventLockCache {
	 
	static Map<String,String> map;

	
	
	static {
		map=new HashMap<String,String>();
	}
	
	public static synchronized boolean lock(String ip,String cmdstr) {
		String t=map.get(ip) ;
		if(t==null) {
			//不存在指令
			map.put(ip, cmdstr);
			return true;
		}else {
			if(t.equals(cmdstr)) {
				//指令相同
				return false;
			}else {
				map.put(ip, cmdstr);
				return true;
			}
		}
	}
}
