package com.lanhai.agv.cache;

/**
 * 对接设备上下料类型
 * @author virus408
 *
 */
public enum ParkTransPosEnum {
	OnlyIn(1),
	OnlyOut(2),
	Both(3),
	Err(4);
	
	
	ParkTransPosEnum(int i) {
		pos=i;
	}

	private int pos;

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}
	
	
	public static ParkTransPosEnum getParkTransPosEnum(int b) {
		ParkTransPosEnum p=null;
		switch(b) {
		case 1:
			p=ParkTransPosEnum.OnlyIn;
			break;
		case 2:
			p=ParkTransPosEnum.OnlyOut;
			break;
		case 3:
			p=ParkTransPosEnum.Both;
			break;
			default:
				p=ParkTransPosEnum.Err;
				break;
		}
		return p;
	}
}
