package com.lanhai.agv.cache;

import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.localstore.TranfficAreaLocalBean;

/**
 * 交管区域
 * 
 * @author virus408
 *
 */
public class TranfficArea {

	private int areaId;

	private int MinX;
	private int MaxX;
	private int MinY;
	private int MaxY;
	private int areaType;
	// 当前锁定的小车 
	private int agvId;
	private List<Integer> stopAgv;

	public TranfficArea() {
		stopAgv = new ArrayList<Integer>();
	}

	public void loadDB(TranfficAreaLocalBean obj) {
		agvId = obj.getAgvId();
		if (obj.getStopAgv() != null && obj.getStopAgv().size() > 0) {
			stopAgv.addAll(obj.getStopAgv());
		}
	}

	public TranfficAreaLocalBean toBean() {
		TranfficAreaLocalBean obj = new TranfficAreaLocalBean();
		obj.setAgvId(agvId);
		obj.setAreaId(areaId);
		obj.setStopAgv(stopAgv);
		return obj;
	}

	public int getAreaId(int x, int y) {
		if (MinX <= x && x <= MaxX && MinY <= y && y <= MaxY) {
			return areaId;
		} else {
			return 0;
		}
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public int getMinX() {
		return MinX;
	}

	public void setMinX(int minX) {
		MinX = minX;
	}

	public int getMaxX() {
		return MaxX;
	}

	public void setMaxX(int maxX) {
		MaxX = maxX;
	}

	public int getMinY() {
		return MinY;
	}

	public void setMinY(int minY) {
		MinY = minY;
	}

	public int getMaxY() {
		return MaxY;
	}

	public void setMaxY(int maxY) {
		MaxY = maxY;
	}

 
	public int getAgvId() {
		return agvId;
	}

	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}

	public List<Integer> getStopAgv() {
		return stopAgv;
	}

	public void setStopAgv(List<Integer> stopAgv) {
		this.stopAgv = stopAgv;
	}

	public int getAreaType() {
		return areaType;
	}

	public void setAreaType(int areaType) {
		this.areaType = areaType;
	}

	/**
	 * 移除停下来的agv
	 * 
	 * @param agvIp
	 */
	public boolean removeAgv(int agvId) {
		if (stopAgv != null && stopAgv.size() > 0) {
			if(stopAgv.contains(agvId)) {
				stopAgv.remove(agvId);
				return true;
			} 
		}
		return false;
	}

	/**
	 * 锁定的可用的下一个agv
	 * 
	 * @return
	 */
	public int nextAgv() {
		if (stopAgv != null && stopAgv.size() > 0) {
			return stopAgv.get(0);
		} else {
			return 0;
		}
	}

	public void addStopAgv(int agvId) {
		if (stopAgv == null)
			stopAgv = new ArrayList<Integer>();
		stopAgv.add(agvId);
	}

}
