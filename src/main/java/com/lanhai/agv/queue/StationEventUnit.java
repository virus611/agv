package com.lanhai.agv.queue;


/**
 *  
 * @author virus408
 *
 */
public class StationEventUnit {
	private byte[] data;
	  private String  ip ;
	  private int port; 
	  
	  
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
 
	  
}
