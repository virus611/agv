package com.lanhai.agv.queue;

/**
 * 调度指令
 * 
 * @author virus408
 *
 */
/**
 * @author virus408
 *
 */
public class DispatchUnit {
	private TargetCmdEnum target;
	private String ip;
	private int port;
	private boolean fromAgv;
	
	//装卸分配点的点位
	private int parkNo;
	//装卸分配点的区段
	//private int lv;
	 
	//关联的工序分组
	private String processName;
	
	//来自后台的数据 
	private byte[] data;

	public TargetCmdEnum getTarget() {
		return target;
	}

	public void setTarget(TargetCmdEnum target) {
		this.target = target;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isFromAgv() {
		return fromAgv;
	}

	public void setFromAgv(boolean fromAgv) {
		this.fromAgv = fromAgv;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getParkNo() {
		return parkNo;
	}

	public void setParkNo(int parkNo) {
		this.parkNo = parkNo;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}
  
}
