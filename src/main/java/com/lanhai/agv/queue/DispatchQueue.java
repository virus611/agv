package com.lanhai.agv.queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 调度指令队列
 * @author virus408
 *
 */
public class DispatchQueue {
	static Queue<DispatchUnit> queue = new LinkedList<DispatchUnit>();

	public static synchronized void Push(DispatchUnit item) {

		queue.offer(item);

	}

	public static synchronized DispatchUnit Pop() {

		if (queue.isEmpty()) {
			return null;
		}
		DispatchUnit item = queue.poll();
		return item;
	}
}
