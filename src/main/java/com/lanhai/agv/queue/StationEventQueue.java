package com.lanhai.agv.queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 对接设备的消息队列 
 * @author virus408
 *
 */
public class StationEventQueue {
	static Queue<StationEventUnit> queue = new LinkedList<StationEventUnit>();

	public static synchronized void Push(StationEventUnit item) {

		queue.offer(item);

	}

	public static synchronized StationEventUnit Pop() {

		if (queue.isEmpty()) {
			return null;
		}
		StationEventUnit item = queue.poll();
		return item;
	}
}
