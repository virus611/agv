package com.lanhai.agv.queue;



/**
 * 内部调度事件类型
 * @author virus408
 *
 */
public enum TargetCmdEnum {
	SelectRoad,Judgement,Nono,StationState,NoticeReadyDot,NoticeEstimate
}
