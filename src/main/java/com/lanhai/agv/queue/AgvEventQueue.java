package com.lanhai.agv.queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 小车的消息队列
 * @author virus408
 *
 */
public class AgvEventQueue {
	static Queue<AgvEventUnit> queue = new LinkedList<AgvEventUnit>();

	public static synchronized void Push(AgvEventUnit item) {

		queue.offer(item);

	}

	public static synchronized AgvEventUnit Pop() {

		if (queue.isEmpty()) {
			return null;
		}
		AgvEventUnit item = queue.poll();
		return item;
	}
}
