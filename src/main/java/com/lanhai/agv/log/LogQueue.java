package com.lanhai.agv.log;
 
import java.util.LinkedList; 
import java.util.Queue;
 

public class LogQueue {
	static Queue<LogData> queue = new LinkedList<LogData>();

	public static synchronized void Push(LogData item) {

		queue.offer(item);

	}

	
	/**
	 * 每次取完
	 * @return
	 */
	public static synchronized  LogData[] Pop() { 
		if (queue.isEmpty()) {
			return null;
		} 
		LogData[]  arr=	 (LogData[]) queue.toArray();
		queue.clear(); 
		return arr;
	}
}
