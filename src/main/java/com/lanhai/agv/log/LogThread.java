package com.lanhai.agv.log;

import java.util.ArrayList;
import java.util.List;

import com.lanhai.agv.Config;

public class LogThread {
	private boolean running;
	private Thread th;
	private LogDBHelper helper=null;
	
	public LogThread(Config conf) {
		try {
			helper=new LogDBHelper(conf.getLogStr(),conf.getLogUser(),conf.getLogPwd());
		} catch (Exception e) {
			 
		}
	}
	
	
	public void Start( ) {
		running = true;
		th = new Thread() {
			@Override
			public void run() {
 
				while (running) {
					LogData[] arr=LogQueue.Pop();
					List<LogData> agvList=new ArrayList<LogData>();
					List<LogData> stationList=new ArrayList<LogData>();
					if(arr!=null) {
						for(LogData logs:arr) {
							//打印，不需要可以注释掉
							System.out.print(logs.toString());
							
							if(logs.getAgvId()==0) {
								//小车
								agvList.add(logs);
							}else {
								stationList.add(logs);
							}
						}  
					} 
					//写数据库，如果不写库，可以注释掉
					if(helper!=null) {
						helper.logAgv(agvList);
						helper.logStation(stationList);
					}
					
					try {
						Thread.sleep(500);
					} catch (Exception ex) {

					}
				}
			}

		};
		th.start();
	}

	public void Stop() {
		running = false;
		try {
			th.interrupt();
			th = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
