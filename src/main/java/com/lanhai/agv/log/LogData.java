package com.lanhai.agv.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.lanhai.agv.Tools;
import com.lanhai.agv.dispatch.AgvData;
import com.lanhai.agv.push.MsgData;
import com.lanhai.agv.thirdpart.StationData;

/**
 * 日志项
 * @author virus408
 *
 */
public class LogData {
	private int agvId;
	private String ip;  
	private String sendRemark;
	private String sendData; 
	private String receiveData;
	private String receiveRemark;
	private String createTime; 
	
	public LogData() {
		Date now=new Date();
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		createTime=formatter.format(now);
	}
	
	public static LogData fromMsgData(MsgData data) {
		LogData obj=new LogData();
		obj.setAgvId(data.getAgvId());
		obj.setIp(data.getIp()); 
		obj.setReceiveData("");
		obj.setReceiveRemark("");
		 
		obj.setSendData(Tools.byte2HexStr(data.getData()));
		obj.setSendRemark(data.getRemark());
		return obj;
	}

	
	
	public static LogData fromAgv (AgvData data,String ip ) {
		LogData obj=new LogData();
		obj.setAgvId(data.getAgvId());
		obj.setIp(ip); 
		obj.setReceiveData( data.getCmdStr());
		obj.setReceiveRemark(data.getType().getName());
		 
		obj.setSendData("");
		obj.setSendRemark("");
		return obj;
	}
	
	public static LogData fromStation(StationData data ,String ip) {
		LogData obj=new LogData();
		obj.setAgvId(0);
		obj.setIp(ip); 
		obj.setReceiveData( data.getCmdStr());
		obj.setReceiveRemark(data.getEventType().getEventName());
		 
		obj.setSendData("");
		obj.setSendRemark("");
		return obj;
	}


	public int getAgvId() {
		return agvId;
	}



	public void setAgvId(int agvId) {
		this.agvId = agvId;
	}



	public String getIp() {
		return ip;
	}



	public void setIp(String ip) {
		this.ip = ip;
	}


 

	public String getSendRemark() {
		return sendRemark;
	}



	public void setSendRemark(String sendRemark) {
		this.sendRemark = sendRemark;
	}



	public String getSendData() {
		return sendData;
	}
 
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public void setSendData(String sendData) {
		this.sendData = sendData;
	}
 
	public String getReceiveData() {
		return receiveData;
	}

  
	public void setReceiveData(String receiveData) {
		this.receiveData = receiveData;
	}



	public String getReceiveRemark() {
		return receiveRemark;
	}



	public void setReceiveRemark(String receiveRemark) {
		this.receiveRemark = receiveRemark;
	}



	@Override
	public String toString() {
		String s;
		if(agvId==0) {
			//station
			if(receiveRemark.isBlank()) {
				//发送
				s=String.format("IP:【%s】,发送指令:【%s】, %s", ip,sendRemark,sendData);
			}else {
				//接收
				s=String.format("IP:【%s】,收到指令:【%s】, %s", ip,receiveRemark,receiveData);
			}
		
		}else {
			//agv
			if(receiveRemark.isBlank()) {
				//发送
				s=String.format("车号:【%d】,发送指令:【%s】, %s", ip,sendRemark,sendData);
			}else {
				//接收
				s=String.format("车号:【%d】,收到指令:【%s】, %s", agvId,receiveRemark,receiveData);
			} 
		} 
		return s;
	}
	
	
	 
}
