package com.lanhai.agv.log;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LogDBHelper {
	String connstr;
	String connuser;
	String connpwd;

	public LogDBHelper(String connStr, String user, String password) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		this.connstr = connStr;
		this.connuser = user;
		this.connpwd = password;

	}
	
	public void logAgv(List<LogData> list) {
		if(list==null||list.size()==0) return;
		List<LogData> tmp=new ArrayList<LogData>();
		String filepath="d:\\agv\\agvlogerr.log";
		for(int i=0;i<list.size();i++) {
			tmp.add(list.get(i));
			if(i%100==0) {
				String sql=createAgvSql(tmp);
				tmp.clear();
				if(doSql(sql)==false) {
					writeFile(filepath,sql);
				} 
			}
		}
		if(tmp.size()>0) {
			String sql=createAgvSql(tmp); 
			if(doSql(sql)==false) {
				writeFile(filepath,sql);
			} 
		}
	}
	
	
	public void logStation(List<LogData> list) {
		if(list==null||list.size()==0) return;
		List<LogData> tmp=new ArrayList<LogData>();
		String filepath="d:\\agv\\stationlogerr.log";
		for(int i=0;i<list.size();i++) {
			tmp.add(list.get(i));
			if(i%100==0) {
				String sql=createStationSql(tmp);
				tmp.clear();
				if(doSql(sql)==false) {
					writeFile(filepath,sql);
				} 
			}
		}
		if(tmp.size()>0) {
			String sql=createStationSql(tmp); 
			if(doSql(sql)==false) {
				writeFile(filepath,sql);
			} 
		}
	}
	
	
	
	private String createAgvSql(List<LogData> list) {
		String before="insert into agvlog(agvid,ip,createtime,receivedata,receiveremark,senddata,sendremark) values";
		String s="";
		for(LogData d :list) {
			s+=String.format(",(%d,'%s','%s','%s','%s','%s','%s')"
					,d.getAgvId(),d.getIp(),d.getCreateTime(),d.getReceiveData(),d.getReceiveRemark(),d.getSendData(),d.getSendRemark());
		}
		s=s.substring(1); 
		return String.format("%s %s;", before,s);
	}
	
	private String createStationSql(List<LogData> list) {
		String before="insert into stationlog(ip,createtime,receivedata,receiveremark,senddata,sendremark) values";
		String s="";
		for(LogData d :list) {
			s+=String.format(",('%s','%s','%s','%s','%s','%s')"
					,d.getIp(),d.getCreateTime(),d.getReceiveData(),d.getReceiveRemark(),d.getSendData(),d.getSendRemark());
		}
		s=s.substring(1); 
		return String.format("%s %s;", before,s);
	}
	

	boolean doSql(String sql) {
		boolean success=false;
		Statement statement = null;
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connstr, connuser, connpwd);
			statement = connection.createStatement();
			statement.execute(sql);
			success=true;
		} catch (Exception er) {
			
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception m1) {
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception m2) {
			}
		}
		return success;
	}

	void writeFile(String filepath, String content) {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filepath, true)));
			out.write(content);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception eg) {

			}
		}
	}
}
